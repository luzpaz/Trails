"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core.interfaces import ICodeCompiler

class Py3FunctionCompiler(ICodeCompiler):
    """Compiles string to python function
    """
    def __init__(self, fooName=None, *args, **kwargs):
        super(Py3FunctionCompiler, self).__init__(*args, **kwargs)
        assert(isinstance(fooName, str))
        self._fooName = fooName

    def compile(self, code):
        """Wraps code to function def

        :param code: Code to wrap
        :type code: :class:`str`
        :returns: Function object
        :rtype: :class:`function`
        """
        foo = "def {}(self):".format(self._fooName)
        lines = [i for i in code.split('\n') if len(i) > 0]
        for line in lines:
            foo += '\n\t{}'.format(line)
        if len(lines) == 0:
            foo += "\n\tpass"
        codeObject = compile(foo, "trails_code_compiler", "exec")
        mem = {}
        exec(codeObject, mem)
        return mem[self._fooName]


class Py3CodeCompiler(ICodeCompiler):
    """Generic python code compiler"""
    def __init__(self):
        super(Py3CodeCompiler, self).__init__()

    def compile(self, code, moduleName="trails_code_compiler", scope={}):
        """Evaluates supplied string

        Used by python node

        :param code: Whatever python code
        :type code: str
        :param moduleName: Used for runtime error messages
        :type moduleName: str
        :param scope: Storage where symbols will be placed
        :type scope: dict
        """
        codeObject = compile(code, moduleName, "exec")
        exec(codeObject, scope)
        return scope
