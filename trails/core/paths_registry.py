"""
Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core.common import SingletonDecorator
from trails.core.graph_manager import GraphManagerSingleton


@SingletonDecorator
class PathsRegistry(object):
    """Holds paths to nodes and pins. Can rebuild paths and return entities by paths."""
    def __init__(self):
        self._data = {}

    def rebuild(self):
        man = GraphManagerSingleton().get()
        allNodes = man.getAllNodes()
        self._data.clear()
        for node in allNodes:
            self._data[node.path()] = node
            for pin in node.pins:
                self._data[pin.path()] = pin

    def getAllPaths(self):
        return list(self._data)

    def contains(self, path):
        return path in self._data

    # def resolvePath(self, base, path):
    #     temp = os.path.normpath(os.path.join(base, path))
    #     res = "/".join(temp.split(os.sep))

    def getEntity(self, path):
        if self.contains(path):
            return self._data[path]
        return None
