"""
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from collections import namedtuple

from trails import __version__


class Version(namedtuple("Version", ("major", "minor", "patch"))):
    """Version class according to `semantic versioning <https://semver.org>`_

    Based on
    - https://packaging.python.org/en/latest/guides/single-sourcing-package-version/,
      option 4.
    - https://docs.python.org/3/library/collections.html#namedtuple-factory-function-for-tuples-with-named-fields,
      subclassing named tuple

    """

    def __new__(cls, *version):
        if not version:
            version = [int(i) for i in __version__.split('.')]
        elif len(version) == 1 and isinstance(version[0], str):
            version = [int(i) for i in version[0].split('.')]

        major, minor, patch = version
        assert(isinstance(major, int))
        assert(isinstance(minor, int))
        assert(isinstance(patch, int))
        return namedtuple("Version", ("major", "minor", "patch")).__new__(cls, *version)

    def __str__(self):
        return "{0}.{1}.{2}".format(self.major, self.minor, self.patch)
