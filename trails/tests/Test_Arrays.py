"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.tests.TestsBase import *
from trails.core.common import NodeBase, connectPins, DEFAULT_IN_EXEC_NAME
from collections import Counter


class TestArrays(unittest.TestCase):

    def setUp(self):
        print('\t[BEGIN TEST]', self._testMethodName)

    def tearDown(self):
        print('--------------------------------\n')

    def test_makeList_Node(self):
        packages = GET_PACKAGES()
        man = GraphManager()
        nodes = packages['TrailsBase'].GetNodeClasses()
        foos = packages['TrailsBase'].GetFunctionLibraries()["DefaultLib"].getFunctions()
        classNodes = packages['TrailsBase'].GetNodeClasses()

        makeListNode = nodes['makeArray']("mkList")
        man.activeGraph().addNode(makeListNode)
        makeIntNode = NodeBase.initializeFromFunction(foos['makeInt'])
        makeIntNode2 = NodeBase.initializeFromFunction(foos['makeInt'])
        man.activeGraph().addNode(makeIntNode)
        man.activeGraph().addNode(makeIntNode2)
        printNode = classNodes["consoleOutput"]("printer")
        man.activeGraph().addNode(printNode)

        connected = connectPins(makeIntNode[str('out')], makeListNode[str('data')])
        connected = connectPins(makeIntNode2[str('out')], makeListNode[str('data')])
        self.assertEqual(connected, True)
        connected = connectPins(makeListNode[str('out')], printNode[str("entity")])
        printNode[DEFAULT_IN_EXEC_NAME].call()
        result = makeListNode[str('out')].getData()
        self.assertEqual(Counter(result), Counter([0, 0]))
