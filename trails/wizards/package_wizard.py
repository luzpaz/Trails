"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import shutil
import platform
import subprocess
import re

from Qt import QtCore
from Qt import QtGui
from Qt.QtWidgets import *

from trails import wizards
from trails.wizards.wizard_dialogue_base import WizardDialogueBase
from trails.wizards.pkg_gen import generatePackage
from trails import packages
from trails.config_manager import PreferencesManager
from trails.packages.trails_base.prefs_widgets.general import cleanPath


# Convert CamelCase to Camel_Case (subsequentially use `.lower()` to get snake_case)
# https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case#answer-12867228
CAMEL_TO_SNAKE = re.compile('((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))')


class PackageWizard(WizardDialogueBase):
    """docstring for PackageWizard."""
    def __init__(self, parent=None):
        super(PackageWizard, self).__init__(parent)

    def addFinalPage(self):
        self.page = QWidget()
        self.pageLayout = QVBoxLayout(self.page)
        self.pageLabel1 = QLabel()
        self.pageLayout.addWidget(self.pageLabel1)
        self.pageLabel2 = QLabel()
        self.pageLayout.addWidget(self.pageLabel2)
        self.cbAddPath = QCheckBox("Add its root path to preferences")
        self.pageLayout.addWidget(self.cbAddPath)
        self.pageLabel3 = QLabel("<strong>Note</strong>: Preferences has to be saved manually!")
        self.pageLayout.addWidget(self.pageLabel3)
        self.pageLayout.addStretch()
        self.cbOpenPath = QCheckBox("Open the package directory")
        self.cbOpenPath.setChecked(True)
        self.pageLayout.addWidget(self.cbOpenPath)
        self.addPageWidget(self.page,
                           'Click "Finish" to generate the package.',
                           titleHtml='Everything is ready!',
                           noteHtml='When the job will be done, you will need to restart Trails in order to see your new stuff.',
                           pageEnterCallback=self.onFinalEntered)

    def onDone(self):
        # if we are here, everything is correct
        packageName = self.packageName.text()
        packageDir = self.packageDir.text()
        packagePath = self.lwOutPathSelect.selectedItems()[0].text()
        packagePath = os.path.expanduser(packagePath)
        includeUINodeFactory = self.cbIncludeUINodeFactory.checkState() == QtCore.Qt.Checked and self.cbIncludeUINodeFactory.isEnabled()
        IncludeUIPinFactory = self.cbUIPinFactory.checkState() == QtCore.Qt.Checked and self.cbUIPinFactory.isEnabled()
        IncludePinInputWidgetFactory = self.cbPinInputWidgetFactory.checkState() == QtCore.Qt.Checked and self.cbPinInputWidgetFactory.isEnabled()
        IncludePrefsWidget = self.cbPrefsWidget.checkState() == QtCore.Qt.Checked
        generatePackage(packageName,
                        packageDir,
                        packagePath,
                        bIncludeClassNode=self.cbIncludeClassNode.checkState() == QtCore.Qt.Checked,
                        bIncludeFooLib=self.cbIncludeFooLib.checkState() == QtCore.Qt.Checked,
                        bIncludeUINodeFactory=includeUINodeFactory,
                        bIncludePin=self.cbIncludePin.checkState() == QtCore.Qt.Checked,
                        bIncludeUIPinFactory=IncludeUIPinFactory,
                        bIncludeTool=self.cbIncludeTool.checkState() == QtCore.Qt.Checked,
                        bIncludeExporter=self.cbIncludeExporter.checkState() == QtCore.Qt.Checked,
                        bIncludePinInputWidgetFactory=IncludePinInputWidgetFactory,
                        bIncludePrefsWindget=IncludePrefsWidget)
        if packagePath not in PreferencesManager().General.ExtraPackageDirs and self.cbAddPath.isChecked():
            PreferencesManager().General.ExtraPackageDirs.append(packagePath)
        if self.cbOpenPath.checkState() == QtCore.Qt.Checked:
            path = os.path.join(packagePath, packageDir)
            system = platform.system()
            if system == "Windows":
                os.startfile(path)
            elif system == "Darwin":
                subprocess.Popen(["open", path])
            elif system == "Linux":
                subprocess.Popen(["xdg-open", path])
        self.accept()

    def pkgNameChanged(self, pkgName):
        self.packageName.setText(pkgName)
        # Convert to CamelCase
        self.packageDir.setText(CAMEL_TO_SNAKE.sub(r'_\1', pkgName).lower())

    def populate(self):
        # first page
        self.p1 = QWidget()
        # self.p1_layout = QHBoxLayout(self.p1)
        # self.p1Layout = QGridLayout(self.p1_layout)
        self.p1Layout = QGridLayout(self.p1)
        # self.p1_layout.addLayout(self.p1Layout)

        self.lePkgName = QLineEdit()
        # allow only valid variable names
        self.lePkgName.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("^[A-Z][a-zA-Z0-9]*$")))
        self.lePkgName.textChanged.connect(self.pkgNameChanged)
        self.p1Layout.addWidget(self.lePkgName, 0, 0, 1, 2)

        self.p1Layout.addWidget(QLabel("Package name:"), 1, 0, QtCore.Qt.AlignLeft)
        self.packageName = QLabel()
        self.p1Layout.addWidget(self.packageName, 1, 1, QtCore.Qt.AlignLeft)

        self.p1Layout.addWidget(QLabel("Subdirectory name:"), 2, 0, QtCore.Qt.AlignLeft)
        self.packageDir = QLabel()
        self.p1Layout.addWidget(self.packageDir, 2, 1, QtCore.Qt.AlignLeft)

        verticalSpacer = QSpacerItem(1, 1, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.p1Layout.addItem(verticalSpacer, 3, 0, QtCore.Qt.AlignTop)
        self.p1Layout.setColumnStretch(1, 1)

        self.lePkgName.setText("DemoPackage")
        self.addPageWidget(self.p1,
                           "Choose a name for your new package:",
                           noteHtml="Please enter the package name in CamelCase.",
                           errorMsgHtml="Please enter a package name!",
                           validationHook=lambda: self.lePkgName.text())

        # second page
        self.p2 = QWidget()
        self.p2Layout = QVBoxLayout(self.p2)

        self.goToDocsWidget = QWidget()
        self.goToDocsLayout = QHBoxLayout(self.goToDocsWidget)
        spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Maximum)
        self.goToDocsLayout.addItem(spacer)
        self.bpGotoComponentsDocs = QPushButton("?")
        self.bpGotoComponentsDocs.setToolTip("Go to docs")
        self.bpGotoComponentsDocs.clicked.connect(self.onGotoComponentsDocs)
        self.goToDocsLayout.addWidget(self.bpGotoComponentsDocs)
        self.p2Layout.addWidget(self.goToDocsWidget)
        self.cbIncludeClassNode = QCheckBox("Class node")
        self.cbIncludeClassNode.stateChanged.connect(self.checkIncludeUINodeFactory)
        self.cbIncludeFooLib = QCheckBox("Function library")
        self.cbIncludePin = QCheckBox("Pin")
        self.cbIncludePin.stateChanged.connect(self.checkUIPinFactories)
        self.cbIncludeTool = QCheckBox("Tool")
        self.cbIncludeExporter = QCheckBox("Exporter")

        self.classNodeLayout = QHBoxLayout()
        self.classNodeLayout.setSpacing(1)
        self.classNodeLayout.setContentsMargins(0, 0, 0, 0)
        self.cbIncludeUINodeFactory = QCheckBox("Include ui node factory")
        self.cbIncludeUINodeFactory.setEnabled(False)
        self.classNodeLayout.addWidget(self.cbIncludeClassNode)
        self.classNodeLayout.addWidget(self.cbIncludeUINodeFactory)
        self.p2Layout.addLayout(self.classNodeLayout)
        self.p2Layout.addWidget(self.cbIncludeFooLib)

        self.pinLayout = QHBoxLayout()
        self.pinLayout.setSpacing(1)
        self.pinLayout.setContentsMargins(0, 0, 0, 0)
        self.cbUIPinFactory = QCheckBox("Include ui pin factory")
        self.cbUIPinFactory.setEnabled(False)
        self.pinLayout.addWidget(self.cbIncludePin)
        self.pinLayout.addWidget(self.cbUIPinFactory)
        self.p2Layout.addLayout(self.pinLayout)

        self.toolLayout = QHBoxLayout()
        self.toolLayout.setSpacing(1)
        self.toolLayout.setContentsMargins(0, 0, 0, 0)
        self.cbPinInputWidgetFactory = QCheckBox("Include pin input widget factory")
        self.cbPinInputWidgetFactory.setEnabled(False)
        self.toolLayout.addWidget(self.cbIncludeTool)
        self.toolLayout.addWidget(self.cbPinInputWidgetFactory)
        self.p2Layout.addLayout(self.toolLayout)

        self.cbPrefsWidget = QCheckBox("Prefs widget")

        self.p2Layout.addWidget(self.cbIncludeExporter)
        self.p2Layout.addWidget(self.cbPrefsWidget)

        self.addPageWidget(self.p2,
                           "Select which components should be included in your new package:",
                           errorMsgHtml="Please select at least one component to be included to package!",
                           validationHook=self.isPackageModuleSelected)

        # third page
        # with a list of extra package dirs and a button to add another one
        self.p3 = QWidget()
        self.p3Layout = QVBoxLayout(self.p3)

        self.p3Label = QLabel()
        self.p3Layout.addWidget(self.p3Label)
        self.lwOutPathSelect = QListWidget()
        self.p3Layout.addWidget(self.lwOutPathSelect)
        self.pbOutPathSelect = QPushButton("Select other directory…")
        self.p3Layout.addWidget(self.pbOutPathSelect)
        self.pbOutPathSelect.clicked.connect(self.onSelectPackageDirectory)
        self.addPageWidget(self.p3,
                           "Select output directory for your new package:",
                           noteHtml="The directory should be writable.",
                           errorMsgHtml="Please select a directory for your new package!",
                           validationHook=lambda: self.lwOutPathSelect.selectedItems(),
                           pageEnterCallback=self.onSelectPackageRootEntered)

    def checkUIPinFactories(self, state):
        checked = self.cbIncludePin.checkState() == QtCore.Qt.Checked
        self.cbPinInputWidgetFactory.setEnabled(checked)
        self.cbUIPinFactory.setEnabled(checked)

    def checkIncludeUINodeFactory(self, state):
        # ui node factories can be created now only for class nodes
        self.cbIncludeUINodeFactory.setEnabled(self.cbIncludeClassNode.checkState() == QtCore.Qt.Checked)

    def onGotoComponentsDocs(self):
        print("Open components docs page")

    def onSelectPackageRootEntered(self):
        self.p3Label.setText(
            f"The package subdirectory '/{self.packageDir.text()}/' will be appended to the selected directory:")
        if self.lwOutPathSelect.count() == 0:
            extraPackageDirs = [cleanPath(path) for path in PreferencesManager().General.ExtraPackageDirs]
            for path in extraPackageDirs:
                self.lwOutPathSelect.addItem(path)
            for path in packages.__path__:
                path = cleanPath(path)
                if path not in extraPackageDirs:
                    self.lwOutPathSelect.addItem(path)
            self.lwOutPathSelect.setCurrentRow(0)

    def isPackageModuleSelected(self):
        return any([self.cbIncludeClassNode.checkState() == QtCore.Qt.Checked,
                    self.cbIncludeFooLib.checkState() == QtCore.Qt.Checked,
                    self.cbIncludePin.checkState() == QtCore.Qt.Checked,
                    self.cbIncludeTool.checkState() == QtCore.Qt.Checked,
                    self.cbIncludeExporter.checkState() == QtCore.Qt.Checked])

    def onSelectPackageDirectory(self, *args):
        packageRoot = QFileDialog.getExistingDirectory(self, "Choose folder", "Choose folder", QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)
        if packageRoot:
            packagePath = cleanPath(packageRoot)
            packageDirs = [self.lwOutPathSelect.item(i).text() for i in range(self.lwOutPathSelect.count())]
            if packagePath in packageDirs:
                self.lwOutPathSelect.setCurrentRow(packageDirs.index(packagePath))
            else:
                pathItem = QListWidgetItem(packagePath)
                self.lwOutPathSelect.addItem(pathItem)
                self.lwOutPathSelect.setCurrentItem(pathItem)

    def onFinalEntered(self):
        packageName = self.packageName.text()
        packageDir = self.packageDir.text()
        packagePath = self.lwOutPathSelect.selectedItems()[0].text()
        self.pageLabel1.setText(
            f"Package name: {packageName}")
        self.pageLabel2.setText(
            f"Package directory: {os.path.join(packagePath, packageDir)}/")
        if packagePath in [cleanPath(path) for path in PreferencesManager().General.ExtraPackageDirs]:
            self.cbAddPath.setEnabled(False)
            self.cbAddPath.setToolTip("Path is already in preferences")
            self.pageLabel3.setEnabled(False)
            self.pageLabel3.setToolTip("Path is already in preferences")
        else:
            self.cbAddPath.setEnabled(True)
            self.cbAddPath.setToolTip("")
            self.pageLabel3.setEnabled(True)
            self.cbAddPath.setToolTip("")

    @staticmethod
    def run():
        instance = PackageWizard()
        instance.exec_()
