"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.ui.tool.tool import ShelfTool
from trails.packages.trails_base.tools import RESOURCES_DIR
from trails.core.common import Direction

from Qt import QtGui
from Qt.QtWidgets import QFileDialog


class AlignBottomTool(ShelfTool):
    """docstring for AlignBottomTool."""
    def __init__(self):
        super(AlignBottomTool, self).__init__()

    @staticmethod
    def toolTip():
        return "Aligns selected nodes by bottom most node"

    @staticmethod
    def getIcon():
        return QtGui.QIcon(RESOURCES_DIR + "alignbottom.png")

    @staticmethod
    def name():
        return str("AlignBottomTool")

    def do(self):
        self.trailsInstance.getCanvas().alignSelectedNodes(Direction.Down)
