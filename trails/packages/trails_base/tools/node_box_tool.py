"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui

from trails.ui.tool.tool import DockTool
from trails.ui.views.node_box import NodesBox


class NodeBoxTool(DockTool):
    """docstring for NodeBox tool."""
    def __init__(self):
        super(NodeBoxTool, self).__init__()

    def onShow(self):
        super(NodeBoxTool, self).onShow()
        self.setMinimumSize(QtCore.QSize(200, 50))
        self.content = NodesBox(self, self.trailsInstance.getCanvas(), False, False, bUseDragAndDrop=True)
        self.content.setObjectName("NodeBoxToolContent")
        self.setWidget(self.content)

    def refresh(self):
        self.content.treeWidget.refresh()

    @staticmethod
    def isSingleton():
        return True

    @staticmethod
    def defaultDockArea():
        return QtCore.Qt.LeftDockWidgetArea

    @staticmethod
    def toolTip():
        return "Available nodes"

    @staticmethod
    def name():
        return str("NodeBox")
