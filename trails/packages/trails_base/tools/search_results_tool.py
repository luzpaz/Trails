"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui
from Qt.QtWidgets import *

from trails.packages.trails_base.tools import RESOURCES_DIR
from trails.ui.tool.tool import DockTool
from trails.ui.widgets.properties_framework import PropertiesWidget, CollapsibleFormWidget


class SearchResultsTool(DockTool):
    """docstring for NodeBox tool."""
    def __init__(self):
        super(SearchResultsTool, self).__init__()
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self.scrollArea = QScrollArea(self)
        self.scrollArea.setWidgetResizable(True)

        self.content = PropertiesWidget()
        self.content.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.actionClear = QAction("Clear", None)
        self.actionClear.triggered.connect(self.content.clear)
        self.content.addAction(self.actionClear)

        self.content.setSearchBoxVisible(False)
        self.content.setLockCheckBoxVisible(False)
        self.content.setTearOffCopyVisible(False)

        self.content.setObjectName("SearchResultstent")
        self.scrollArea.setWidget(self.content)
        self.setWindowTitle(self.uniqueName())
        self.setWidget(self.scrollArea)

    def onShowNodesResults(self, uiNodesList):
        self.content.clear()
        category = CollapsibleFormWidget(headName="Results")
        category.setSpacing(0)
        for node in uiNodesList:
            locationString = ">".join(node.location())
            btn = QPushButton(locationString)
            btn.clicked.connect(lambda checked=False, n=node: self.trailsInstance.getCanvas().frameItems([n]))
            category.addWidget(node.getName(), btn)
        self.content.addWidget(category)

    @staticmethod
    def defaultDockArea():
        return QtCore.Qt.BottomDockWidgetArea

    def onShow(self):
        super(SearchResultsTool, self).onShow()
        self.trailsInstance.getCanvas().requestShowSearchResults.connect(self.onShowNodesResults)

    @staticmethod
    def toolTip():
        return "Available nodes"

    @staticmethod
    def isSingleton():
        return True

    @staticmethod
    def name():
        return str("Search results")
