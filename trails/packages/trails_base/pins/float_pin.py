"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import PinBase


class FloatPin(PinBase):
    """doc string for FloatPin"""

    def __init__(self, name, parent, direction, **kwargs):
        super(FloatPin, self).__init__(name, parent, direction, **kwargs)
        self.setDefaultValue(0.0)

    @staticmethod
    def IsValuePin():
        return True

    @staticmethod
    def pinDataTypeHint():
        '''data type index and default value'''
        return 'FloatPin', 0.0

    @staticmethod
    def color():
        return (96, 169, 23, 255)

    @staticmethod
    def supportedDataTypes():
        return ('FloatPin', 'IntPin',)

    @staticmethod
    def internalDataStructure():
        return float

    @staticmethod
    def processData(data):
        return FloatPin.internalDataStructure()(data)
