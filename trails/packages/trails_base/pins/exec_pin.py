"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import PinBase
from trails.core.common import PinOptions, PinDirection, currentProcessorTime


# Execution pin
class ExecPin(PinBase):
    def __init__(self, name, parent, direction, **kwargs):
        super(ExecPin, self).__init__(name, parent, direction, **kwargs)
        self.dirty = False
        self._isArray = False
        if self.direction == PinDirection.Input:
            self.enableOptions(PinOptions.AllowMultipleConnections)
        self._lastCallTime = 0.0

    def isExec(self):
        return True

    def pinConnected(self, other):
        super(ExecPin, self).pinConnected(other)

    def setAsArray(self, bIsArray):
        # exec is not a type, it cannot be an list
        self._isArray = False

    @staticmethod
    def IsValuePin():
        return False

    @staticmethod
    def supportedDataTypes():
        return ('ExecPin',)

    @staticmethod
    def pinDataTypeHint():
        return 'ExecPin', None

    @staticmethod
    def internalDataStructure():
        return None

    @staticmethod
    def color():
        return (200, 200, 200, 255)

    def setData(self, data):
        pass

    def getLastExecutionTime(self):
        return self._lastCallTime

    def call(self, *args, **kwargs):
        if self.owningNode().isValid():
            self._lastCallTime = currentProcessorTime()
        super(ExecPin, self).call(*args, **kwargs)
