"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType
from trails.packages.trails_base.nodes import FLOW_CONTROL_COLOR


class charge(NodeBase):
    def __init__(self, name):
        super(charge, self).__init__(name)
        self.inExec = self.createInputPin('inExec', 'ExecPin', None, self.compute)
        self.amount = self.createInputPin('Amount', 'FloatPin')
        self.amount.setDefaultValue(1.0)

        self.step = self.createInputPin('Step', 'FloatPin')
        self.step.setDefaultValue(0.1)

        self.completed = self.createOutputPin('completed', 'ExecPin')
        self._currentAmount = 0
        self.headerColor = FLOW_CONTROL_COLOR

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('FloatPin')
        helper.addOutputDataType('ExecPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'FlowControl'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return 'Each time node called it accumulates the step value.' +\
               'When accumulated value reaches <b>amount</b> - <b>completed</b> pin called.' +\
               'Useful when you need to wait some time inside some tick function.'

    def compute(self, *args, **kwargs):
        step = abs(self.step.getData())
        if (self._currentAmount + step) < abs(self.amount.getData()):
            self._currentAmount += step
            return
        self.completed.call(*args, **kwargs)
        self._currentAmount = 0.0
