"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core.common import StructureType, DEFAULT_IN_EXEC_NAME
from trails.core.paths_registry import PathsRegistry
from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.packages.trails_base.nodes import FLOW_CONTROL_ORANGE


class loopEnd(NodeBase):
    def __init__(self, name):
        super(loopEnd, self).__init__(name)
        self.inExec = self.createInputPin(DEFAULT_IN_EXEC_NAME, 'ExecPin', None, self.compute)
        self.loopBeginNode = self.createInputPin('Paired block', 'StringPin')
        self.loopBeginNode.setInputWidgetVariant("ObjectPathWIdget")
        self.completed = self.createOutputPin('Completed', 'ExecPin')
        self.headerColor = FLOW_CONTROL_ORANGE
        self.setExperimental()

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addInputDataType('StringPin')
        helper.addInputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'FlowControl'

    @staticmethod
    def keywords():
        return ['iter', 'end']

    @staticmethod
    def description():
        return 'For loop end block'

    def compute(self, *args, **kwargs):
        node = PathsRegistry().getEntity(self.loopBeginNode.getData())
        if node is not None:
            if node.graph() == self.graph():
                if node.loopEndNode.getData() != self.path():
                    self.setError("Invalid pair")
                    return
                if node.__class__.__name__ == "forLoopBegin":
                    node.prevIndex = node.currentIndex
                    node.currentIndex += 1
                    if node.currentIndex >= node.lastIndex.getData():
                        self.completed.call()
                else:
                    node.onNext()
            else:
                err = "block ends in different graphs"
                node.setError(err)
                self.setError(err)
        else:
            self.setError("Pair {} not found".format(self.loopBeginNode.getData()))
