"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType, pinAffects


class rerouteExecs(NodeBase):
    def __init__(self, name):
        super(rerouteExecs, self).__init__(name)
        self.input = self.createInputPin("in", 'ExecPin')
        self.output = self.createOutputPin("out", 'ExecPin')
        pinAffects(self.input, self.output)
        self.input.call = self.output.call

    def postCreate(self, jsonTemplate=None):
        super(rerouteExecs, self).postCreate(jsonTemplate=jsonTemplate)
        self.setName("reroute")

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('ExecPin')
        helper.addOutputDataType('ExecPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'Common'

    def compute(self, *args, **kwargs):
        pass
