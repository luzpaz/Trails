"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2021  biek
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from blinker import Signal

from trails.core import NodeBase
from trails.core.common import StructureType, PinOptions, getUniqNameFromList, uuid


class graphInputs(NodeBase):
    """Represents a group of input pins on compound node
    """
    def __init__(self, name):
        super(graphInputs, self).__init__(name)
        self.bCacheEnabled = True

    def getUniqPinName(self, name):
        result = name
        graphNodes = self.graph().getNodesList(classNameFilters=['graphInputs', 'graphOutputs'])
        conflictingPinNames = set()
        for node in graphNodes:
            for pin in node.pins:
                conflictingPinNames.add(pin.name)
        result = getUniqNameFromList(conflictingPinNames, name)
        return result

    @staticmethod
    def category():
        return 'SubGraphs'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return ''

    def addOutPin(self, name=None, dataType="AnyPin"):
        # called after postCreate was executed
        if name is None:
            name = self.getUniqPinName('in')
        p = self.createOutputPin(name, dataType, constraint=name, structConstraint=name, structure=StructureType.Multi)
        p.enableOptions(PinOptions.RenamingEnabled | PinOptions.Dynamic)
        if dataType == "AnyPin":
            p.enableOptions(PinOptions.AllowAny | PinOptions.DictElementSupported)
        return p


    def compute(self, *args, **kwargs):
        for o in self.outputs.values():
            for i in o.affected_by:
                if len(i.affected_by)>0:
                    for e in i.affected_by:
                        o.setData(e.getData())
                else:
                    o.setData(i.getData())

    def postCreate(self, jsonTemplate=None):
        # called after node was added to graph
        super(graphInputs, self).postCreate(jsonTemplate=jsonTemplate)
        # recreate dynamically created pins
        existingPins = self.namePinOutputsMap
        if jsonTemplate is not None:
            sortedOutputs = sorted(jsonTemplate["outputs"], key=lambda x: x["pinIndex"])
            for outPinJson in sortedOutputs:
                if outPinJson['name'] not in existingPins:
                    dynOut = self.addOutPin(outPinJson['name'], outPinJson["dataType"])
                    dynOut.uid = uuid.UUID(outPinJson['uuid'])


class graphOutputs(NodeBase):
    """Represents a group of output pins on compound node
    """
    def __init__(self, name):
        super(graphOutputs, self).__init__(name)
        self.bCacheEnabled = False

    def getUniqPinName(self, name):
        result = name
        graphNodes = self.graph().getNodesList(classNameFilters=['graphInputs', 'graphOutputs'])
        conflictingPinNames = set()
        for node in graphNodes:
            for pin in node.pins:
                conflictingPinNames.add(pin.name)
        result = getUniqNameFromList(conflictingPinNames, name)
        return result

    @staticmethod
    def category():
        return 'SubGraphs'

    @staticmethod
    def keywords():
        return []

    @staticmethod
    def description():
        return ''

    def postCreate(self, jsonTemplate=None):
        super(graphOutputs, self).postCreate(jsonTemplate=jsonTemplate)
        # recreate dynamically created pins
        existingPins = self.namePinInputsMap
        if jsonTemplate is not None:
            sortedInputs = sorted(jsonTemplate["inputs"], key=lambda x: x["pinIndex"])
            for inPinJson in sortedInputs:
                if inPinJson['name'] not in existingPins:
                    inDyn = self.addInPin(inPinJson['name'], inPinJson["dataType"])
                    inDyn.uid = uuid.UUID(inPinJson['uuid'])

    def addInPin(self, name=None, dataType="AnyPin"):
        if name is None:
            name = self.getUniqPinName('out')
        p = self.createInputPin(name, dataType, constraint=name, structConstraint=name, structure=StructureType.Multi)
        p.enableOptions(PinOptions.RenamingEnabled | PinOptions.Dynamic)
        if dataType == "AnyPin":
            p.enableOptions(PinOptions.AllowAny | PinOptions.DictElementSupported)
        return p

    def compute(self, *args, **kwargs):
        compoundNode = None
        for i in self.inputs.values():
            for o in i.affects:
                compoundNode = o.owningNode()
                o.setDirty()
        if compoundNode:
            compoundNode.processNode()
