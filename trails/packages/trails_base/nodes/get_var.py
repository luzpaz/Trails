"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2021  Cemsina Güzel
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from copy import copy
import uuid

from trails.packages.trails_base import PACKAGE_NAME
from trails.core import NodeBase
from trails.core.variable import Variable
from trails.core.common import StructureType, PinOptions, push
from trails import CreateRawPin


class getVar(NodeBase):
    def __init__(self, name, var=None):
        super(getVar, self).__init__(name)
        assert(isinstance(var, Variable))
        self._var = var
        if var.structure == StructureType.Dict:
            self.out = self.createOutputPin('out', var.value.valueType, structure=StructureType.Dict, constraint="2")
        else:
            self.out = self.createOutputPin('out', var.dataType)
        self.out.disableOptions(PinOptions.RenamingEnabled)

        self._var.valueChanged.connect(self.onVarValueChanged)
        self._var.structureChanged.connect(self.onVarStructureChanged)
        self._var.dataTypeChanged.connect(self.onDataTypeChanged)
        self.bCacheEnabled = False

    def checkForErrors(self):
        super(getVar, self).checkForErrors()
        if self._var is None:
            self.setError("Undefined variable")

    def onDataTypeChanged(self, dataType):
        self.recreateOutput(dataType)
        self.checkForErrors()
        wrapper = self.getWrapper()
        if wrapper:
            wrapper.onVariableWasChanged()

    def updateStructure(self):
        self.out.disconnectAll()
        if self._var.structure == StructureType.Single:
            self.out.setAsArray(False)
        if self._var.structure == StructureType.Array:
            self.out.setAsArray(True)
        if self._var.structure == StructureType.Dict:
            self.out.setAsDict(True)
            self.out.updateConnectedDicts([], self._var.value.keyType)

    def onVarStructureChanged(self, newStructure):
        self.out.structureType = newStructure
        self.updateStructure()

    def recreateOutput(self, dataType):
        self.out.kill()
        del self.out
        self.out = None
        self.out = CreateRawPin('out', self, dataType, PinDirection.Output)
        self.out.disableOptions(PinOptions.RenamingEnabled)
        self.updateStructure()
        return self.out

    @property
    def var(self):
        return self._var

    @var.setter
    def var(self, newVar):
        if self._var is not None:
            self._var.dataTypeChanged.disconnect(self.onDataTypeChanged)
            self._var.structureChanged.disconnect(self.onVarStructureChanged)
            self._var.valueChanged.disconnect(self.onVarValueChanged)
        self._var = newVar
        if newVar is not None:
            self._var.valueChanged.connect(self.onVarValueChanged)
            self._var.structureChanged.connect(self.onVarStructureChanged)
            self._var.dataTypeChanged.connect(self.onDataTypeChanged)
            self.recreateOutput(self._var.dataType)
        else:
            # self.out.kill()
            # del self.out
            # self.out = None
            pass
        self.checkForErrors()
        wrapper = self.getWrapper()
        if wrapper:
            wrapper.onVariableWasChanged()

    def postCreate(self, jsonTemplate=None):
        super(getVar, self).postCreate(jsonTemplate)
        self.updateStructure()

    def variableUid(self):
        return self.var.uid

    def onVarValueChanged(self, *args, **kwargs):
        push(self.out)

    def serialize(self):
        default = NodeBase.serialize(self)
        if self.var is not None:
            default['varUid'] = str(self.var.uid)
        return default

    @staticmethod
    def category():
        return PACKAGE_NAME

    @staticmethod
    def keywords():
        return ["get", "var"]

    @staticmethod
    def description():
        return 'Access variable value'

    def compute(self, *args, **kwargs):
        self.out.setData(copy(self.var.value))
