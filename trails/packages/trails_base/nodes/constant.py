"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import StructureType, PinOptions, pinAffects
from trails import findPinClassByType, getAllPinClasses, CreateRawPin
from copy import copy

class constant(NodeBase):
    def __init__(self, name):
        super(constant, self).__init__(name)
        self.input = self.createInputPin("in", 'AnyPin', defaultValue=0.0, structure=StructureType.Multi, constraint="1", structConstraint="1")
        self.output = self.createOutputPin("out", 'AnyPin', defaultValue=0.0, structure=StructureType.Multi, constraint="1", structConstraint="1")
        self.input.disableOptions(PinOptions.ChangeTypeOnConnection)
        self.output.disableOptions(PinOptions.ChangeTypeOnConnection)
        pinAffects(self.input, self.output)
        self.input.call = self.output.call
        self.pinTypes = []
        for pinClass in getAllPinClasses():
            if pinClass.IsValuePin() and pinClass.__name__ != "AnyPin":
                self.pinTypes.append(pinClass.__name__)
        self.bCacheEnabled = False

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('AnyPin')
        helper.addOutputDataType('AnyPin')
        helper.addInputStruct(StructureType.Multi)
        helper.addOutputStruct(StructureType.Multi)
        return helper

    @staticmethod
    def category():
        return 'GenericTypes'

    @staticmethod
    def keywords():
        return ["Make"]


    def serialize(self):
        orig = super(constant, self).serialize()
        orig["currDataType"] = self.input.dataType
        return orig

    def postCreate(self, jsonTemplate=None):
        super(constant, self).postCreate(jsonTemplate)
        if "currDataType" in jsonTemplate:
            self.updateType(self.pinTypes.index(jsonTemplate["currDataType"]))

    def onPinConected(self,other):
        self.changeType(other.dataType)

    def overrideTypeChanged(self,toogle):
        if bool(toogle):
            self.input.enableOptions(PinOptions.ChangeTypeOnConnection)
            self.output.enableOptions(PinOptions.ChangeTypeOnConnection)
        else:
            self.input.disableOptions(PinOptions.ChangeTypeOnConnection)
            self.output.disableOptions(PinOptions.ChangeTypeOnConnection)

    def updateType(self,dataTypeIndex):
        self.input.enableOptions(PinOptions.ChangeTypeOnConnection)
        self.output.enableOptions(PinOptions.ChangeTypeOnConnection)
        self.changeType(self.pinTypes[dataTypeIndex],True)
        self.input.disableOptions(PinOptions.ChangeTypeOnConnection)
        self.output.disableOptions(PinOptions.ChangeTypeOnConnection)

    def changeType(self,dataType,init=False):
        a = self.input.initType(dataType,init)
        b = self.output.initType(dataType,init)

    def selectStructure(self,name):
        self.input.changeStructure(StructureType(name),True)
        self.output.changeStructure(StructureType(name),True)


    def compute(self, *args, **kwargs):
        self.output.setData(self.input.getData())
