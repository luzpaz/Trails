"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import NodeBase
from trails.core.node_base import NodePinsSuggestionsHelper
from trails.core.common import pinAffects, PinOptions, StructureType
from trails import findPinClassByType, getAllPinClasses, CreateRawPin
from copy import copy


class convertTo(NodeBase):
    def __init__(self, name):
        super(convertTo, self).__init__(name)
        self.input = self.createInputPin("in", 'AnyPin', defaultValue=None)
        self.output = self.createOutputPin("result", 'AnyPin', defaultValue=None)
        pinAffects(self.input, self.output)
        self.input.enableOptions(PinOptions.AllowAny)
        self.pinTypes = []
        for pinClass in getAllPinClasses():
            if pinClass.IsValuePin():
                self.pinTypes.append(pinClass.__name__)

    @staticmethod
    def pinTypeHints():
        helper = NodePinsSuggestionsHelper()
        helper.addInputDataType('AnyPin')
        helper.addOutputDataType('AnyPin')
        helper.addInputStruct(StructureType.Single)
        helper.addOutputStruct(StructureType.Single)
        return helper

    @staticmethod
    def category():
        return 'GenericTypes'

    def serialize(self):
        orig = super(convertTo, self).serialize()
        orig["currDataType"] = self.output.dataType
        return orig

    def postCreate(self, jsonTemplate=None):
        super(convertTo, self).postCreate(jsonTemplate)
        if "currDataType" in jsonTemplate:
            self.updateType(self.pinTypes.index(jsonTemplate["currDataType"]))

    def updateType(self, dataTypeIndex):
        self.output.enableOptions(PinOptions.ChangeTypeOnConnection)
        self.changeType(self.pinTypes[dataTypeIndex], True)
        self.output.disableOptions(PinOptions.ChangeTypeOnConnection)

    def changeType(self, dataType, init=False):
        b = self.output.initType(dataType, init)

    def compute(self, *args, **kwargs):
        otherClass = findPinClassByType(self.output.dataType)
        self.output.setData(otherClass.processData(self.input.getData()))
