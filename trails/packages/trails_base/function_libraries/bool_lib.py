"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails.core.common import NodeMeta


class BoolLib(FunctionLibraryBase):
    '''doc string for BoolLib'''
    def __init__(self, packageName):
        super(BoolLib, self).__init__(packageName)

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Math|Bool', NodeMeta.KEYWORDS: []})
    def boolAnd(a=('BoolPin', False), b=('BoolPin', False)):
        '''Returns the logical `AND` of two values `(A AND B)`.'''
        return a and b

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Math|Bool', NodeMeta.KEYWORDS: []})
    def boolNot(a=('BoolPin', False)):
        '''Returns the logical complement of the Boolean value `(NOT A)`.'''
        return not a

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Math|Bool', NodeMeta.KEYWORDS: []})
    def boolNand(a=('BoolPin', False), b=('BoolPin', False)):
        '''Returns the logical `NAND` of two values `(A AND B)`.'''
        return not (a and b)

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Math|Bool', NodeMeta.KEYWORDS: []})
    def boolNor(a=('BoolPin', False), b=('BoolPin', False)):
        '''Returns the logical `Not OR` of two values `(A NOR B)`.'''
        return not (a or b)

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Math|Bool', NodeMeta.KEYWORDS: []})
    def boolOr(a=('BoolPin', False), b=('BoolPin', False)):
        '''Returns the logical `OR` of two values `(A OR B)`.'''
        return a or b

    @staticmethod
    @IMPLEMENT_NODE(returns=('BoolPin', False), meta={NodeMeta.CATEGORY: 'Math|Bool', NodeMeta.KEYWORDS: []})
    def boolXor(a=('BoolPin', False), b=('BoolPin', False)):
        '''Returns the logical `eXclusive OR` of two values `(A XOR B)`.'''
        return a ^ b
