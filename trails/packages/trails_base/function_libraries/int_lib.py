"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.core import(
    FunctionLibraryBase,
    IMPLEMENT_NODE
)
from trails.core.common import NodeMeta, sign


class IntLib(FunctionLibraryBase):
    """doc string for IntLib"""
    def __init__(self, packageName):
        super(IntLib, self).__init__(packageName)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def bitwiseAnd(a=('IntPin', 0), b=('IntPin', 0)):
        """Bitwise AND <var>(A &amp; B)</var>"""
        return a & b

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def bitwiseNot(a=('IntPin', 0)):
        """Bitwise NOT <var>(~A)</var>"""
        return ~a

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def bitwiseOr(a=('IntPin', 0), b=('IntPin', 0)):
        """Bitwise OR <var>(A | B)</var>"""
        return a | b

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def bitwiseXor(a=('IntPin', 0), b=('IntPin', 0)):
        """Bitwise XOR <var>(A ^ B)</var>"""
        return a ^ b

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def binaryLeftShift(a=('IntPin', 0), b=('IntPin', 0)):
        """Binary left shift <var>a &lt;&lt; b</var>"""
        return a << b

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def binaryRightShift(a=('IntPin', 0), b=('IntPin', 0)):
        """Binary right shift <var>a &gt;&gt; b</var>"""
        return a >> b

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def testBit(intType=('IntPin', 0), offset=('IntPin', 0)):
        """Returns a nonzero result, 2**offset, if the bit at 'offset' is one"""
        mask = 1 << offset
        return(intType & mask)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def setBit(intType=('IntPin', 0), offset=('IntPin', 0)):
        """Returns an integer with the bit at 'offset' set to 1."""
        mask = 1 << offset
        return(intType | mask)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def clearBit(intType=('IntPin', 0), offset=('IntPin', 0)):
        """Returns an integer with the bit at 'offset' cleared."""
        mask = ~(1 << offset)
        return(intType & mask)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Bits manipulation', NodeMeta.KEYWORDS: []})
    def toggleBit(intType=('IntPin', 0), offset=('IntPin', 0)):
        """Returns an integer with the bit at 'offset' inverted, 0 -&gt; 1 and 1 -&gt; 0."""
        mask = 1 << offset
        return(intType ^ mask)

    @staticmethod
    @IMPLEMENT_NODE(returns=('IntPin', 0), meta={NodeMeta.CATEGORY: 'Math|Int', NodeMeta.KEYWORDS: []})
    def sign(x=('IntPin', 0)):
        """Sign (integer, returns -1 if A &lt; 0, 0 if A is zero, and +1 if A &gt; 0)"""
        return int(sign(x))
