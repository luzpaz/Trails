"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.packages.trails_base.nodes.switch_on_string import switchOnString
from trails.packages.trails_base.nodes.get_var import getVar
from trails.packages.trails_base.nodes.set_var import setVar
from trails.packages.trails_base.nodes.sequence import sequence
from trails.packages.trails_base.nodes.python_node import pythonNode
from trails.packages.trails_base.nodes.comment_node import commentNode
from trails.packages.trails_base.nodes.sticky_note import stickyNote
from trails.packages.trails_base.nodes.reroute import reroute
from trails.packages.trails_base.nodes.reroute_execs import rerouteExecs
from trails.packages.trails_base.nodes.graph_nodes import (
    graphInputs,
    graphOutputs
)
from trails.packages.trails_base.nodes.float_ramp import floatRamp
from trails.packages.trails_base.nodes.color_ramp import colorRamp

from trails.packages.trails_base.nodes.compound import compound
from trails.packages.trails_base.nodes.constant import constant
from trails.packages.trails_base.nodes.convert_to import convertTo
from trails.packages.trails_base.nodes.make_dict import makeDict
from trails.packages.trails_base.nodes.make_any_dict import makeAnyDict

from trails.packages.trails_base.nodes.for_loop_begin import forLoopBegin
from trails.packages.trails_base.nodes.while_loop_begin import whileLoopBegin

from trails.packages.trails_base.nodes.image_display import imageDisplay
from trails.packages.trails_base.ui.ui_image_display_node import UIImageDisplayNode

from trails.packages.trails_base.ui.ui_switch_on_string_node import UISwitchOnString
from trails.packages.trails_base.ui.ui_get_var_node import UIGetVarNode
from trails.packages.trails_base.ui.ui_set_var_node import UISetVarNode
from trails.packages.trails_base.ui.ui_sequence_node import UISequenceNode
from trails.packages.trails_base.ui.ui_comment_node import UICommentNode
from trails.packages.trails_base.ui.ui_sticky_note import UIStickyNote
from trails.packages.trails_base.ui.ui_reroute_node_small import UIRerouteNodeSmall
from trails.packages.trails_base.ui.ui_python_node import UIPythonNode
from trails.packages.trails_base.ui.ui_graph_nodes import (
    UIGraphInputs,
    UIGraphOutputs
)
from trails.packages.trails_base.ui.ui_float_ramp import UIFloatRamp
from trails.packages.trails_base.ui.ui_color_ramp import UIColorRamp

from trails.packages.trails_base.ui.ui_compound_node import UICompoundNode
from trails.packages.trails_base.ui.ui_constant_node import UIConstantNode
from trails.packages.trails_base.ui.ui_convert_to_node import UIConvertToNode
from trails.packages.trails_base.ui.ui_make_dict_node import UIMakeDictNode
from trails.packages.trails_base.ui.ui_for_loop_begin_node import UIForLoopBeginNode
from trails.packages.trails_base.ui.ui_while_loop_begin_node import UIWhileLoopBeginNode

from trails.ui.canvas.ui_node_base import UINodeBase


def createUINode(raw_instance):
    if isinstance(raw_instance, getVar):
        return UIGetVarNode(raw_instance)
    if isinstance(raw_instance, setVar):
        return UISetVarNode(raw_instance)
    if isinstance(raw_instance, switchOnString):
        return UISwitchOnString(raw_instance)
    if isinstance(raw_instance, sequence):
        return UISequenceNode(raw_instance)
    if isinstance(raw_instance, commentNode):
        return UICommentNode(raw_instance)
    if isinstance(raw_instance, stickyNote):
        return UIStickyNote(raw_instance)
    if isinstance(raw_instance, reroute) or isinstance(raw_instance, rerouteExecs):
        return UIRerouteNodeSmall(raw_instance)
    if isinstance(raw_instance, graphInputs):
        return UIGraphInputs(raw_instance)
    if isinstance(raw_instance, graphOutputs):
        return UIGraphOutputs(raw_instance)
    if isinstance(raw_instance, compound):
        return UICompoundNode(raw_instance)
    if isinstance(raw_instance, pythonNode):
        return UIPythonNode(raw_instance)
    if isinstance(raw_instance, constant):
        return UIConstantNode(raw_instance)
    if isinstance(raw_instance, convertTo):
        return UIConvertToNode(raw_instance)
    if isinstance(raw_instance, makeDict):
        return UIMakeDictNode(raw_instance)
    if isinstance(raw_instance, makeAnyDict):
        return UIMakeDictNode(raw_instance)
    if isinstance(raw_instance, floatRamp):
        return UIFloatRamp(raw_instance)
    if isinstance(raw_instance, colorRamp):
        return UIColorRamp(raw_instance)
    if isinstance(raw_instance, imageDisplay):
        return UIImageDisplayNode(raw_instance)
    if isinstance(raw_instance,forLoopBegin):
        return UIForLoopBeginNode(raw_instance)
    if isinstance(raw_instance,whileLoopBegin):
        return UIWhileLoopBeginNode(raw_instance)
    return UINodeBase(raw_instance)
