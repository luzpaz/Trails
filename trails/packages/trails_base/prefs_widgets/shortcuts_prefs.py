"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import json
from collections import defaultdict

from Qt.QtWidgets import *

from trails.config_manager import ShortcutsManager
from trails.input import InputAction
from trails.ui.widgets.properties_framework import CollapsibleFormWidget, PropertiesWidget
from trails.ui.widgets.preferences_window import CategoryWidgetBase
from trails.ui.widgets.input_action_widget import InputActionWidget
from trails.ui.canvas.ui_common import clearLayout


class ShortcutsPreferences(CategoryWidgetBase):
    """docstring for ShortcutsPreferences."""

    manager = ShortcutsManager()

    def __init__(self, parent=None):
        super(ShortcutsPreferences, self).__init__(parent)
        self.content = QWidget()
        self.layout = QVBoxLayout(self.content)
        self.layout.setContentsMargins(1, 1, 1, 1)
        self.layout.setSpacing(2)
        self.setWidget(self.content)

    def serialize(self):
        # FIXME: This is not needed?
        raise NotImplementedError
        # data = ShortcutsManager().serialize()
        # return data
        # with open(ConfigManager().INPUT_CONFIG_PATH, "w") as f:
        #     json.dump(data, f, indent=4)

    def update_manager(self):
        return self.manager

    def onShow(self):
        clearLayout(self.layout)
        properties = PropertiesWidget()
        properties.setLockCheckBoxVisible(False)
        properties.setTearOffCopyVisible(False)

        for actionArea, actionGroups in ShortcutsManager().items():
            for actionGroup, actions in actionGroups.items():
                category = CollapsibleFormWidget(headName=f'{actionArea}/{actionGroup}')
                for actionName, actionsVariants in actions.items():
                    for inputActionVariant in actionsVariants:
                        actionWidget = InputActionWidget(
                            inputActionRef=inputActionVariant)
                        actionWidget.setAction(inputActionVariant)
                        category.addWidget(
                            label=inputActionVariant.getName(),
                            widget=actionWidget, maxLabelWidth=150)
                properties.addWidget(category)
                category.setCollapsed(True)
        self.layout.addWidget(properties)

        spacerItem = QSpacerItem(10, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.layout.addItem(spacerItem)
