"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtWidgets

from trails.config_manager import PreferencesManager, UIThemeManager, GraphThemeManager
from trails.ui.widgets.preferences_window import CategoryWidgetBase
from trails.ui.widgets.properties_framework import CollapsibleFormWidget


SYSTEM_DEFAULT_NAME = 'System Default'


class _ThemePreferences(CategoryWidgetBase):
    """Preferences for themes."""

    manager = PreferencesManager()
    section = None
    theme_manager = None
    # DEFAULT_PREFERENCES = {
    #     # TODO: None
    #     'Theme': None}

    def __init__(self, parent=None):
        super().__init__(parent)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.setContentsMargins(1, 1, 1, 1)
        self.layout.setSpacing(2)

        self.themesSelector = QtWidgets.QComboBox()

        category = CollapsibleFormWidget(headName='Common')
        category.addWidget('Theme', self.themesSelector)
        self.layout.addWidget(category)
        self.themesSelector.activated.connect(self.setTheme)

        spacerItem = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.layout.addItem(spacerItem)

    def onShow(self):
        """Called when the screen is shown."""
        # Populate the drop-down box
        self.themesSelector.clear()
        for name in sorted(self.theme_manager.themes, key=lambda x: x or ''):
            if name is None:
                name = SYSTEM_DEFAULT_NAME
            self.themesSelector.addItem(name)

        # Get theme name from `manager`
        theme = self.manager[self.section].Theme
        if theme is None:
            theme = SYSTEM_DEFAULT_NAME

        # Select entry
        for index in range(self.themesSelector.count()):
            if self.themesSelector.itemText(index) == theme:
                break
        else:
            index = 0
        self.themesSelector.setCurrentIndex(index)

    def setTheme(self, index):
        """Callback function, when a new theme is selected

        Parameters
        ----------
        index : int
            The index of the selected item of the themes' drop down box.

        Returns
        -------
        None.

        """
        name = self.themesSelector.currentText()
        if name == SYSTEM_DEFAULT_NAME:
            name = None
        self.theme_manager.select(name)
        self.theme_manager.updateApp()

    def serialize(self):
        """Serializes the settings

        Returns
        -------
        dict
            The dictionary with the "Themes" key.

        """
        theme = self.themesSelector.currentText()
        if theme == SYSTEM_DEFAULT_NAME:
            theme = None
        return {'Theme': theme}


class UIThemePreferences(_ThemePreferences):
    """Preferences for user interface theme screen."""

    section = 'UI'
    theme_manager = UIThemeManager()


class GraphThemePreferences(_ThemePreferences):
    """Preferences for canvas theme screen."""

    section = 'Graph'
    theme_manager = GraphThemeManager()
