"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import uuid
from Qt import QtGui
from Qt import QtCore
from Qt.QtWidgets import *

from trails.core.node_base import NodeBase
from trails.ui.canvas.painters import NodePainter
from trails.ui.canvas.ui_node_base import UINodeBase
from trails.ui.canvas.i_convex_hull_back_drop import IConvexHullBackDrop


class UIForLoopBeginNode(UINodeBase, IConvexHullBackDrop):
    def __init__(self, raw_node):
        super(UIForLoopBeginNode, self).__init__(raw_node)
        IConvexHullBackDrop.__init__(self)

    def postCreate(self, jsonTemplate=None):
        super(UIForLoopBeginNode, self).postCreate(jsonTemplate)
        self.scene().addItem(self.backDrop)
        self.computeHull()
        self.backDrop.update()

    def eventDropOnCanvas(self):
        # TODO: try to simplify this with Canvas.spawnNode
        nodeTemplate = NodeBase.jsonTemplate()
        nodeTemplate['package'] = "TrailsBase"
        nodeTemplate['lib'] = ""
        nodeTemplate['type'] = "loopEnd"
        nodeTemplate['name'] = self.canvasRef().graphManager.getUniqNodeName("loopEnd")
        nodeTemplate['x'] = self.scenePos().x() + self.geometry().width() + 30
        nodeTemplate['y'] = self.scenePos().y()
        nodeTemplate['uuid'] = str(uuid.uuid4())
        endNode = self.canvasRef()._createNode(nodeTemplate)
        self.getPinSG("Paired block").setData(str(endNode.path()))
        endNode.getPinSG("Paired block").setData(self.path())
        self.canvasRef().connectPins(self.getPinSG("LoopBody"), endNode.getPinSG(DEFAULT_IN_EXEC_NAME))

    def paint(self, painter, option, widget):
        self.computeHull()
        self.backDrop.update()
        NodePainter.default(self, painter, option, widget)
