"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtGui
from trails.ui import RESOURCES_DIR
from trails.ui.canvas.ui_node_base import UINodeBase
from Qt.QtWidgets import QLabel


class UIImageDisplayNode(UINodeBase):
    def __init__(self, raw_node):
        super(UIImageDisplayNode, self).__init__(raw_node)
        self.resizable = True
        self.Imagelabel = QLabel("test3")
        self.pixmap = QtGui.QPixmap(RESOURCES_DIR + "/wizard-cat.png")
        self.addWidget(self.Imagelabel)
        self.updateSize()
        self._rawNode.loadImage.connect(self.onLoadImage)

    def onLoadImage(self, imagePath):
        self.pixmap = QtGui.QPixmap(imagePath)
        self.updateSize()

    def paint(self, painter, option, widget):
        self.updateSize()
        super(UIImageDisplayNode, self).paint(painter, option, widget)

    def updateSize(self):
        scaledPixmap = self.pixmap.scaledToWidth(
            self.customLayout.geometry().width())
        self.Imagelabel.setPixmap(scaledPixmap)
