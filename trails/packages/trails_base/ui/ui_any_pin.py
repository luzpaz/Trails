"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails import findPinClassByType
from trails.ui.widgets.select_pin_dialog import SelectPinDialog
from trails.ui.canvas.ui_pin_base import UIPinBase
from Qt import QtGui


class UIAnyPin(UIPinBase):
    def __init__(self, owningNode, raw_pin):
        """UI wrapper for :class:`trails.packages.trails_base.pins.any_pin`

        :param owningNode: Owning node
        :type owningNode: :class:`trails.ui.canvas.node_base`
        :param raw_pin: PinBase reference
        :type raw_pin: :class:`trails.packages.trails_base.pins.any_pin`
        """
        super(UIAnyPin, self).__init__(owningNode, raw_pin)
        self._defaultColor = self._pinColor
        self._rawPin.typeChanged.connect(self.setType)
        self._rawPin.dataTypeBeenSet.connect(self.dataTypeBeenSet)
        self._rawPin.onPinDisconnected.connect(self.disconnect)
        self.menu.addAction("InitAs").triggered.connect(self.selectInit)

        self.prevDataType = "AnyPin"
        self.prevColor = None

    def dataTypeBeenSet(self, *args, **kwargs):
        self.prevColor = None
        self.prevDataType = None
        self.setDefault(self._rawPin.defColor())

    def checkFree(self, checked=[], selfCheck=True):
        return self._rawPin.checkFree(checked, selfCheck)

    def disconnect(self, other):
        self.prevColor = None
        self.prevDataType = None

    @property
    def activeDataType(self):
        return self._rawPin.activeDataType

    def setDefault(self, defcolor):
        if defcolor != self.prevColor:
            self.prevColor = defcolor
            self._pinColor = QtGui.QColor(*defcolor)
            for e in self.connections:
                e.setColor(QtGui.QColor(*defcolor))
            self.OnPinChanged.emit(self)
            self.update()

    def selectInit(self):
        validPins = list(self._rawPin._defaultSupportedDataTypes)
        if "AnyPin" in validPins:
            validPins.remove("AnyPin")
        self.d = SelectPinDialog(validPins=validPins)
        self.d.exec_()
        dataType = self.d.getResult()
        if dataType is not None:
            self.initType(dataType, True)

    def initType(self, dataType, init=False):
        self._rawPin.initType(dataType, init)

    def setType(self, dataType):
        if dataType != self.prevDataType:
            self.prevDataType = dataType
            colorTuple = findPinClassByType(dataType).color()
            self._pinColor = QtGui.QColor(*colorTuple)
            for e in self.connections:
                e.setColor(self._pinColor)
            self.OnPinChanged.emit(self)
            self.update()
