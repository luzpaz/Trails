"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore

from trails.ui.canvas.ui_common import NodeActionButtonInfo
from trails.ui.canvas.ui_node_base import UINodeBase
from trails.ui.widgets.select_pin_dialog import SelectPinDialog
from trails.core.graph_manager import GraphManagerSingleton
from trails.ui import RESOURCES_DIR
from trails.config_manager import GraphThemeManager


class UIGraphInputs(UINodeBase):
    pinCreated = QtCore.Signal(object)

    def __init__(self, raw_node):
        super(UIGraphInputs, self).__init__(raw_node)
        actionAddOut = self._menu.addAction("Add pin")
        actionAddOut.setData(NodeActionButtonInfo(RESOURCES_DIR + "/pin.svg"))
        actionAddOut.triggered.connect(self.createPinDialog)
        self.color = GraphThemeManager().NodeAlt
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.image = RESOURCES_DIR + "/gear.svg"

    def setName(self, name):
        oldName = self.getName()
        super(UIGraphInputs, self).setName(name)
        owningCompoundNode = self.canvasRef().graphManager.findNode(
            self._rawNode.graph().name)
        if owningCompoundNode:
            uiCompoundNode = owningCompoundNode.getWrapper()
            if oldName in uiCompoundNode.groups["input"]:
                grpItem = uiCompoundNode.groups["input"][oldName]
                grpItem.name = name
            if oldName in owningCompoundNode.groups["input"]:
                for inp in owningCompoundNode.groups["input"][oldName]:
                    inp.grop = name
                owningCompoundNode.groups["input"][name] = owningCompoundNode.groups["input"].pop(oldName)

    def createPinDialog(self):
        self.d = SelectPinDialog()
        self.d.exec_()
        dataType = self.d.getResult()
        if dataType is not None:
            self.onAddOutPin(None, dataType)

    def onAddOutPin(self, name=None, dataType="AnyPin"):
        rawPin = self._rawNode.addOutPin(name, dataType)
        uiPin = self._createUIPinWrapper(rawPin)
        uiPin.labelColor = GraphThemeManager().PinName
        self.pinCreated.emit(uiPin)
        self.updateNodeShape()
        return uiPin

    def postCreate(self, jsonTemplate):
        # this call will create wrappers for raw pins
        UINodeBase.postCreate(self, jsonTemplate)

        for uiPin in self.UIPins.values():
            uiPin.labelColor = GraphThemeManager().PinName

    def createInputWidgets(self, inputsCategory, inGroup=None, pins=True):
        if self.graph() == GraphManagerSingleton().get().findRootGraph():
            self.createOutputWidgets(inputsCategory, inGroup)

class UIGraphOutputs(UINodeBase):
    pinCreated = QtCore.Signal(object)

    def __init__(self, raw_node):
        super(UIGraphOutputs, self).__init__(raw_node)
        actionAddOut = self._menu.addAction("Add pin")
        actionAddOut.setData(NodeActionButtonInfo(RESOURCES_DIR + "/pin.svg"))
        actionAddOut.triggered.connect(self.createPinDialog)

        self.color = GraphThemeManager().NodeAlt
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.image = RESOURCES_DIR + "/gear.svg"

    def setName(self, name):
        oldName = self.getName()
        super(UIGraphOutputs, self).setName(name)
        owningCompoundNode = self.canvasRef().graphManager.findNode(
            self._rawNode.graph().name)
        if owningCompoundNode:
            uiCompoundNode = owningCompoundNode.getWrapper()
            if oldName in uiCompoundNode.groups["output"]:
                grpItem = uiCompoundNode.groups["output"][oldName]
                grpItem.name = name

    def createPinDialog(self):
        self.d = SelectPinDialog()
        self.d.exec_()
        dataType = self.d.getResult()
        if dataType is not None:
            self.onAddInPin(None, dataType)

    def onAddInPin(self, name=None, dataType="AnyPin"):
        rawPin = self._rawNode.addInPin(name, dataType)
        uiPin = self._createUIPinWrapper(rawPin)
        uiPin.labelColor = GraphThemeManager().PinName
        self.pinCreated.emit(uiPin)
        self.updateNodeShape()
        return uiPin

    def postCreate(self, jsonTemplate):
        UINodeBase.postCreate(self, jsonTemplate)
        for uiPin in self.UIPins.values():
            uiPin.labelColor = GraphThemeManagers().PinName
