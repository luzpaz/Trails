"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.ui import RESOURCES_DIR
from trails.ui.canvas.ui_node_base import UINodeBase
from trails.ui.canvas.ui_common import NodeActionButtonInfo


class UISequenceNode(UINodeBase):
    def __init__(self, raw_node):
        super(UISequenceNode, self).__init__(raw_node)
        actionAddOut = self._menu.addAction("Add out pin")
        actionAddOut.setData(NodeActionButtonInfo(RESOURCES_DIR + "/pin.svg"))
        actionAddOut.setToolTip("Adds output execution pin")
        actionAddOut.triggered.connect(self.onAddOutPin)

    def onPinWasKilled(self, uiPin):
        index = 1
        uiPin.OnPinDeleted.disconnect(self.onPinWasKilled)
        pins = list(self.UIoutputs.values())
        pins.sort(key=lambda x: int(x._rawPin.name))
        for outPin in pins:
            outPin.setName(str(index), True)
            outPin.setDisplayName("Then {0}".format(index))
            index += 1

    def postCreate(self, jsonTemplate=None):
        super(UISequenceNode, self).postCreate(jsonTemplate)
        for outPin in self.UIoutputs.values():
            outPin.setDisplayName("Then {0}".format(outPin._rawPin.name))
            outPin.OnPinDeleted.connect(self.onPinWasKilled)

    def onAddOutPin(self):
        rawPin = self._rawNode.createOutputPin()
        uiPin = self._createUIPinWrapper(rawPin)
        uiPin.OnPinDeleted.connect(self.onPinWasKilled)
        uiPin.setDisplayName("Then {0}".format(rawPin.name))
        return uiPin
