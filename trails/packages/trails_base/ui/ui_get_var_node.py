"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from trails.ui import RESOURCES_DIR
from trails.ui.canvas.ui_node_base import UINodeBase
from trails.core.common import PinOptions, getConnectedPins
from trails.ui.canvas.painters import NodePainter
from trails.ui.widgets.properties_framework import CollapsibleFormWidget
from trails.ui.widgets.enum_combo_box import EnumComboBox
from trails.config_manager import GraphThemeManager



# Variable getter node
class UIGetVarNode(UINodeBase):
    def __init__(self, raw_node):
        super(UIGetVarNode, self).__init__(raw_node)
        self.image = RESOURCES_DIR + "/gear.svg"
        self.headColorOverride = GraphThemeManager().NodeHeadAlt
        self.color = GraphThemeManager().NodeAlt

    def onVariableWasChanged(self):
        if self.var is not None:
            self._createUIPinWrapper(self._rawNode.out)

    @property
    def var(self):
        return self._rawNode.var

    @var.setter
    def var(self, newVar):
        self._rawNode.var = newVar

    def postCreate(self, jsonTemplate=None):
        super(UIGetVarNode, self).postCreate(jsonTemplate)

        self.updateNodeShape()

        self.var.nameChanged.connect(self.onVarNameChanged)

        outPin = list(self._rawNode.pins)[0]
        outPin.setName(self.var.name)

        pinWrapper = outPin.getWrapper()
        if pinWrapper:
            pinWrapper().setMenuItemEnabled("InitAs", False)
            outPin.disableOptions(PinOptions.RenamingEnabled)
            pinWrapper().syncRenamable()
        self.updateHeaderText()

    def serialize(self):
        template = UINodeBase.serialize(self)
        template['meta']['var'] = self.var.serialize()
        return template

    def onVarSelected(self, varName):
        if self.var is not None:
            if self.var.name == varName:
                return
        else:
            self._rawNode.out.disconnectAll()

        var = self.canvasRef().graphManager.findVariableByName(varName)
        free = self._rawNode.out.checkFree([])

        if var:
            linkedTo = getConnectedPins(self._rawNode.out)
            self.var = var
            self._rawNode.updateStructure()
            for i in linkedTo:
                if i.isAny():
                    i.setDefault()
                self.canvasRef().connectPinsInternal(self._rawNode.out.getWrapper()(), i.getWrapper()())
            self.updateHeaderText()
        self.canvasRef().trailsInstance.onRequestFillProperties(self.createPropertiesWidget)
        self._rawNode.checkForErrors()
        self.update()

    def createInputWidgets(self, inputsCategory, inGroup=None, pins=True):
        validVars = self.graph().getVarList()
        cbVars = EnumComboBox([v.name for v in validVars])
        if self.var is not None:
            cbVars.setCurrentText(self.var.name)
        else:
            cbVars.setCurrentText("")
        cbVars.changeCallback.connect(self.onVarSelected)
        inputsCategory.addWidget("var", cbVars, group=inGroup)

    def updateHeaderText(self):
        self.setHeaderHtml("Get {0}".format(self.var.name))
        self.updateNodeShape()

    def onVarNameChanged(self, newName):
        self.updateHeaderText()

    def paint(self, painter, option, widget):
        NodePainter.default(self, painter, option, widget)
