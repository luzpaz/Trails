"""
Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt.QtWidgets import *

class FileDialog(QFileDialog):
    """docstring for ExecInputWidget"""
    def __init__(self, mode="all", multifile=False, parent=None, **kwds):
        super(FileDialog, self).__init__(parent=parent, **kwds)
        self.setOption(QFileDialog.DontUseNativeDialog, True)
        if mode == "all":
            self.setFileMode(QFileDialog.Directory)
            for but in self.findChildren(QPushButton):
                if "open" in but.text().lower() or "choose" in but.text().lower():
                    but.clicked.disconnect()
                    but.clicked.connect(self.chooseClicked)

        elif mode == "file":
            self.setFileMode(QFileDialog.ExistingFile)

        elif mode == "directory":
            self.setFileMode(QFileDialog.DirectoryOnly)

        if multifile:
            self.listView = self.findChild(QListView)
            if self.listView:
                self.listView.setSelectionMode(QAbstractItemView.ExtendedSelection)
            self.treeView = self.findChild(QTreeView)
            if self.treeView:
                self.treeView.setSelectionMode(QAbstractItemView.ExtendedSelection)

    def chooseClicked(self):
        QDialog.accept(self)
