"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt.QtWidgets import *
from Qt import QtCore, QtGui


class KeyboardModifiersCaptureWidget(QPushButton):
    """docstring for KeyboardModifiersCaptureWidget."""
    captured = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(KeyboardModifiersCaptureWidget, self).__init__(parent)
        self._currentModifiers = QtCore.Qt.NoModifier
        self.setText("NoModifier")
        self.bCapturing = False
        self.setCheckable(True)
        self.setToolTip("""
            <b>Left click</b> to start/stop capturing:<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>Enter</b> to accept.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>Esc</b> to exit.<br/>
            <b>Right click</b> to clear.""")

        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.actionClear = QAction("Clear", None)
        self.actionClear.triggered.connect(self.clearModifier)
        self.addAction(self.actionClear)

    def clearModifier(self):
        self.currentModifiers = QtCore.Qt.NoModifier
        self.bCapturing = False
        self.setChecked(False)

    @staticmethod
    def modifiersToString(modifiers):
        if modifiers == QtCore.Qt.KeyboardModifier.NoModifier:
            return "NoModifier"
        return QtGui.QKeySequence(modifiers).toString()[:-1]

    def mousePressEvent(self, event):
        super(KeyboardModifiersCaptureWidget, self).mousePressEvent(event)
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            if not self.bCapturing:
                self.bCapturing = True
                self.setText("capturing…")
            else:
                self.bCapturing = False
                self.setText(self.modifiersToString(self._currentModifiers))

    @property
    def currentModifiers(self):
        return self._currentModifiers

    @currentModifiers.setter
    def currentModifiers(self, value):
        self._currentModifiers = value
        self.setText(self.modifiersToString(self._currentModifiers))
        self.captured.emit(self._currentModifiers)

    def keyPressEvent(self, event):
        super(KeyboardModifiersCaptureWidget, self).keyPressEvent(event)
        key = event.key()
        if key == QtCore.Qt.Key_Escape:
            self.bCapturing = False
            self.setChecked(False)
            self.setText(self.modifiersToString(self._currentModifiers))
            return

        if key == QtCore.Qt.Key_Return and self.bCapturing:
            self.bCapturing = False
            self.setChecked(False)
            self.update()

        if self.bCapturing:
            self.currentModifiers = event.modifiers()


if __name__ == "__main__":
    import sys
    a = QApplication(sys.argv)

    w = KeyboardModifiersCaptureWidget()
    w.show()

    sys.exit(a.exec_())
