"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt.QtWidgets import *
from Qt import QtCore, QtGui


class MouseButtonCaptureWidget(QPushButton):
    """docstring for MouseButtonCaptureWidget."""
    captured = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(MouseButtonCaptureWidget, self).__init__(parent)
        self._currentButton = QtCore.Qt.MouseButton.NoButton
        self.setText(self._currentButton.name.decode('utf-8'))
        self.bCapturing = False
        self.setCheckable(True)
        self.setToolTip("""
            <b>Left click</b> to start capturing:<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>Esc</b> to exit.<br/>
            <b>Right click</b> to clear.""")

        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.actionClear = QAction("Clear", None)
        self.actionClear.triggered.connect(self.clearButton)
        self.addAction(self.actionClear)

    def clearButton(self):
        self.currentButton = QtCore.Qt.MouseButton.NoButton

    @property
    def currentButton(self):
        return self._currentButton

    @currentButton.setter
    def currentButton(self, btn):
        self._currentButton = btn
        self.setText(self._currentButton.name.decode('utf-8'))
        self.setChecked(False)
        self.bCapturing = False
        self.captured.emit(self._currentButton)

    def keyPressEvent(self, event):
        super(MouseButtonCaptureWidget, self).keyPressEvent(event)
        if event.key() == QtCore.Qt.Key_Escape:
            self.bCapturing = False
            self.setChecked(False)
            self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
            self.setText(self._currentButton.name.decode('utf-8'))

    def mousePressEvent(self, event):
        button = event.button()
        if self.bCapturing:
            # capture mouse button
            self.currentButton = button
            # FIXME: This opens the context menu, but the menu should not show!
            self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        elif button == QtCore.Qt.MouseButton.LeftButton:
            self.bCapturing = True
            self.setText("capturing…")
            self.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
            super(MouseButtonCaptureWidget, self).mousePressEvent(event)


if __name__ == "__main__":
    import sys
    a = QApplication(sys.argv)

    w = MouseButtonCaptureWidget()
    w.show()

    sys.exit(a.exec_())
