"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from collections import OrderedDict


class ContextMenuDataBuilder(object):
    """docstring for ContextMenuDataBuilder."""
    def __init__(self):
        super(ContextMenuDataBuilder, self).__init__()
        self._storage = OrderedDict()
        self._menu = []

    def addSeparator(self):
        self._menu.append({"separator": True})

    def addEntry(self, name, title, callback=None, icon=None, parentEntry=None):
        if name not in self._menu:

            menu = OrderedDict()
            menu['name'] = name
            menu['title'] = title
            menu['icon'] = icon
            menu['callback'] = callback
            self._storage[name] = menu

            if parentEntry is not None and parentEntry in self._storage:
                self._storage[parentEntry]["sub_menu"] = menu
            else:
                self._menu.append(menu)
                self._storage[name] = menu

        return self

    def reset(self):
        self._storage.clear()
        self.menu.clear()

    def get(self):
        return self._menu
