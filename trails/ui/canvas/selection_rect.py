"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtGui, QtWidgets, QtCore


class SelectionRect(QtWidgets.QGraphicsWidget):
    __backgroundColor = QtGui.QColor(100, 100, 100, 50)
    __backgroundAddColor = QtGui.QColor(0, 100, 0, 50)
    __backgroundSubColor = QtGui.QColor(100, 0, 0, 50)
    __backgroundSwitchColor = QtGui.QColor(0, 0, 100, 50)
    __pen = QtGui.QPen(QtGui.QColor(255, 255, 255), 1.0, QtCore.Qt.DashLine)

    def __init__(self, graph, mouseDownPos, modifiers):
        super(SelectionRect, self).__init__()
        self.setZValue(2)

        self.__graph = graph
        self.__graph.scene().addItem(self)
        self.__mouseDownPos = mouseDownPos
        self.__modifiers = modifiers
        self.setPos(self.__mouseDownPos)
        self.resize(0, 0)
        self.selectFullyIntersectedItems = False

    def collidesWithItem(self, item):
        if self.selectFullyIntersectedItems:
            return self.sceneBoundingRect().contains(item.sceneBoundingRect())
        return super(SelectionRect, self).collidesWithItem(item)

    def setDragPoint(self, dragPoint, modifiers):
        self.__modifiers = modifiers
        topLeft = QtCore.QPointF(self.__mouseDownPos)
        bottomRight = QtCore.QPointF(dragPoint)
        if dragPoint.x() < self.__mouseDownPos.x():
            topLeft.setX(dragPoint.x())
            bottomRight.setX(self.__mouseDownPos.x())
        if dragPoint.y() < self.__mouseDownPos.y():
            topLeft.setY(dragPoint.y())
            bottomRight.setY(self.__mouseDownPos.y())
        self.setPos(topLeft)
        self.resize(bottomRight.x() - topLeft.x(),
                    bottomRight.y() - topLeft.y())

    def paint(self, painter, option, widget):
        rect = self.windowFrameRect()
        if self.__modifiers == QtCore.Qt.NoModifier:
            painter.setBrush(self.__backgroundColor)
        if self.__modifiers == QtCore.Qt.ShiftModifier:
            painter.setBrush(self.__backgroundAddColor)
        elif self.__modifiers == QtCore.Qt.ControlModifier:
            painter.setBrush(self.__backgroundSwitchColor)
        elif self.__modifiers == QtCore.Qt.ControlModifier | QtCore.Qt.ShiftModifier:
            painter.setBrush(self.__backgroundSubColor)
        painter.setPen(self.__pen)
        painter.drawRect(rect)

    def destroy(self):
        self.__graph.scene().removeItem(self)
