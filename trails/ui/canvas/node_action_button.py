"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore, QtGui
from Qt import QtSvg
from Qt.QtWidgets import QGraphicsWidget
from Qt.QtWidgets import QSizePolicy


class NodeActionButtonBase(QGraphicsWidget):
    """Base class for all node's actions buttons.

    By default it calls action `triggered` signal. Have default svg 10x10 icon.
    """
    def __init__(self, svgFilePath, action, uiNode):
        super(NodeActionButtonBase, self).__init__(uiNode)
        self.setAcceptHoverEvents(True)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setGraphicsItem(self)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum))
        self.action = action
        self.svgIcon = QtSvg.QGraphicsSvgItem(svgFilePath, self)
        self.setToolTip(self.action.toolTip())
        self.hovered = False
        uiNode._actionButtons.add(self)

    def hoverEnterEvent(self, event):
        self.hovered = True
        self.update()

    def hoverLeaveEvent(self, event):
        self.hovered = False
        self.update()

    def mousePressEvent(self, event):
        if self.parentItem().isSelected():
            self.parentItem().setSelected(False)

        if self.action is not None and self.hasFocus():
            self.action.triggered.emit()
            self.clearFocus()

    def setGeometry(self, rect):
        self.prepareGeometryChange()
        super(QGraphicsWidget, self).setGeometry(rect)
        self.setPos(rect.topLeft())

    def sizeHint(self, which, constraint):
        return QtCore.QSizeF(10, 10)
