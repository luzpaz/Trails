"""
Copyright (C) 2015-2019  ?? Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from Qt import QtCore
from Qt import QtGui
from Qt.QtWidgets import *

from trails.core.common import GetRangePct, lerp
from trails.ui.canvas.ui_common import CanvasManipulationMode
from trails.config_manager import GraphThemeManager


class CanvasBase(QGraphicsView):

    _manipulationMode = CanvasManipulationMode.NONE
    _mouseWheelZoomRate = 0.0005

    def __init__(self):
        super(CanvasBase, self).__init__()
        self.pressed_item = None
        self.released_item = None
        self.factor = 1
        self._minimum_scale = 0.2
        self._maximum_scale = 3.0
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        # Antialias -- Change to Settings
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setRenderHint(QtGui.QPainter.TextAntialiasing)
        ##
        self.setAcceptDrops(True)
        self.setAttribute(QtCore.Qt.WA_AlwaysShowToolTips)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)

        self.setScene(self.createScene())

        self.scene().setSceneRect(QtCore.QRectF(0, 0, 10, 10))

        self.mousePressPose = QtCore.QPointF(0, 0)
        self.mousePos = QtCore.QPointF(0, 0)
        self._lastMousePos = QtCore.QPointF(0, 0)

        self.centerOn(QtCore.QPointF(self.sceneRect().width() / 2, self.sceneRect().height() / 2))

    def createScene(self):
        scene = QGraphicsScene(self)
        scene.setItemIndexMethod(QGraphicsScene.NoIndex)
        scene.setSceneRect(QtCore.QRectF(0, 0, 10, 10))
        return scene

    def getItemsRect(self, cls=QGraphicsItem, bSelectedOnly=False, bVisibleOnly=True):
        rectangles = []
        for item in self.scene().items():
            if isinstance(item, cls):
                if bVisibleOnly and not item.isVisible():
                    continue
                if bSelectedOnly and not item.isSelected():
                    continue

                rect = item.sceneBoundingRect().toRect()
                rectangles.append(rect)

        result = QtCore.QRect()

        for r in rectangles:
            result |= r

        return result

    def frameItems(self, items):
        rect = QtCore.QRect()
        for i in items:
            rect |= i.sceneBoundingRect().toRect()
        self.frameRect(rect)

    @property
    def manipulationMode(self):
        return self._manipulationMode

    @manipulationMode.setter
    def manipulationMode(self, value):
        self._manipulationMode = value
        if value == CanvasManipulationMode.NONE:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)
        elif value == CanvasManipulationMode.SELECT:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)
        elif value == CanvasManipulationMode.PAN:
            self.viewport().setCursor(QtCore.Qt.OpenHandCursor)
        elif value == CanvasManipulationMode.MOVE:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)
        elif value == CanvasManipulationMode.ZOOM:
            self.viewport().setCursor(QtCore.Qt.SizeHorCursor)
        elif value == CanvasManipulationMode.COPY:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)

    def wheelEvent(self, event):
        (xfo, invRes) = self.transform().inverted()
        topLeft = xfo.map(self.rect().topLeft())
        bottomRight = xfo.map(self.rect().bottomRight())
        center = (topLeft + bottomRight) * 0.5
        zoomFactor = 1.0 + event.delta() * self._mouseWheelZoomRate

        self.zoom(zoomFactor)

    def zoom(self, scale_factor):
        self.factor = self.transform().m22()
        futureScale = self.factor * scale_factor
        if futureScale <= self._minimum_scale:
            scale_factor = (self._minimum_scale) / self.factor
        if futureScale >= self._maximum_scale:
            scale_factor = (self._maximum_scale - 0.1) / self.factor
        self.scale(scale_factor, scale_factor)

    def setZoom(self, zoom):
        factor = self.transform().m22()
        if factor and zoom:
            scale_factor = zoom / factor
            self.scale(scale_factor, scale_factor)

    def setAutoZoom(self, adj=1):
        app = QApplication.instance()
        screen = app.primaryScreen()
        scale = screen.physicalDotsPerInch() / screen.logicalDotsPerInch()
        self.setZoom(scale * adj)

    def frameRect(self, rect):
        if rect is None:
            return
        windowRect = self.mapToScene(self.rect()).boundingRect()

        # pan to center of window
        delta = windowRect.center() - rect.center()
        delta *= self.currentViewScale()
        self.pan(delta)

        # zoom to fit content
        ws = windowRect.size()
        rect += QtCore.QMargins(40, 40, 40, 40)
        rs = rect.size()
        widthRef = ws.width()
        heightRef = ws.height()
        sx = widthRef / rect.width()
        sy = heightRef / rect.height()
        scale = sx if sy > sx else sy
        self.zoom(scale)

        return scale

    def zoomDelta(self, direction):
        if direction:
            self.zoom(1 + 0.1)
        else:
            self.zoom(1 - 0.1)

    def pan(self, delta):
        rect = self.sceneRect()
        scale = self.currentViewScale()
        x = -delta.x() / scale
        y = -delta.y() / scale
        rect.translate(x, y)
        self.setSceneRect(rect)
        self.update()

    def resetScale(self):
        self.resetMatrix()

    def viewMinimumScale(self):
        return self._minimum_scale

    def viewMaximumScale(self):
        return self._maximum_scale

    def currentViewScale(self):
        return self.transform().m22()

    def getLodValueFromScale(self, numLods=5, scale=1.0):
        lod = lerp(numLods, 1, GetRangePct(self.viewMinimumScale(), self.viewMaximumScale(), scale))
        return int(round(lod))

    def getLodValueFromCurrentScale(self, numLods=5):
        return self.getLodValueFromScale(numLods, self.currentViewScale())

    def getCanvasLodValueFromCurrentScale(self):
        return self.getLodValueFromScale(GraphThemeManager().CanvasLODNumber, self.currentViewScale())

    def drawBackground(self, painter, rect):
        super(CanvasBase, self).drawBackground(painter, rect)
        lod = self.getCanvasLodValueFromCurrentScale()
        self.boundingRect = rect

        polygon = self.mapToScene(self.viewport().rect())

        painter.fillRect(rect, GraphThemeManager().CanvasBackground)

        left = int(rect.left()) - (int(rect.left()) % GraphThemeManager().GridSizeFine)
        top = int(rect.top()) - (int(rect.top()) % GraphThemeManager().GridSizeFine)

        if GraphThemeManager().GridDraw:
            if lod < GraphThemeManager().GridSwitch:
                # Set pen
                pen = QtGui.QPen()
                pen.setWidth(0)
                pen.setStyle(QtCore.Qt.DotLine)
                pen.setColor(GraphThemeManager().GridFine)
                painter.setPen(pen)

                # Draw horizontal fine lines
                gridLines = []
                y = float(top)
                while y < float(rect.bottom()):
                    gridLines.append(QtCore.QLineF(rect.left(), y, rect.right(), y))
                    y += GraphThemeManager().GridSizeFine
                painter.drawLines(gridLines)

                # Draw vertical fine lines
                gridLines = []
                x = float(left)
                while x < float(rect.right()):
                    gridLines.append(QtCore.QLineF(x, rect.top(), x, rect.bottom()))
                    x += GraphThemeManager().GridSizeFine
                painter.drawLines(gridLines)

            # Set pen
            pen = QtGui.QPen()
            pen.setWidth(0)
            pen.setStyle(QtCore.Qt.SolidLine)
            pen.setColor(GraphThemeManager().GridCoarse)
            painter.setPen(pen)

            # Draw thick grid
            left = int(rect.left()) - (int(rect.left()) % GraphThemeManager().GridSizeCoarse)
            top = int(rect.top()) - (int(rect.top()) % GraphThemeManager().GridSizeCoarse)

            # Draw vertical thick lines
            gridLines = []
            x = left
            while x < rect.right():
                gridLines.append(QtCore.QLineF(x, rect.top(), x, rect.bottom()))
                x += GraphThemeManager().GridSizeCoarse
            painter.drawLines(gridLines)

            # Draw horizontal thick lines
            gridLines = []
            y = top
            while y < rect.bottom():
                gridLines.append(QtCore.QLineF(rect.left(), y, rect.right(), y))
                y += GraphThemeManager().GridSizeCoarse
            painter.drawLines(gridLines)

        if GraphThemeManager().GridDrawNumbers:
            # draw numbers
            scale = self.currentViewScale()
            f = painter.font()
            f.setPointSize(6 / min(scale, 1))
            f.setFamily("Consolas")
            painter.setFont(f)
            y = float(top)

            while y < float(rect.bottom()):
                y += GraphThemeManager().GridSizeCoarse
                inty = int(y)
                if y > top + 30:
                    painter.setPen(QtGui.QPen(GraphThemeManager().GridNumbers))
                    painter.drawText(rect.left(), y - 1.0, str(inty))

            x = float(left)
            while x < rect.right():
                x += GraphThemeManager().GridSizeCoarse
                intx = int(x)
                if x > left + 30:
                    painter.setPen(QtGui.QPen(GraphThemeManager().GridNumbers))
                    painter.drawText(x, rect.top() + painter.font().pointSize(), str(intx))
