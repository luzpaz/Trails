"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import uuid
from Qt import QtWidgets
from Qt import QtGui, QtCore

from trails.config_manager import UIThemeManager


class ToolBase(object):
    """Base class for all editor tools

    .. py:method:: name()
        :staticmethod:

        Returns name of this tool
    """

    packageName = ""  #: Package name this tool belongs to

    def __init__(self):
        super(ToolBase, self).__init__()
        self.uid = uuid.uuid4()
        self.trailsInstance = None

    @staticmethod
    def supportedSoftwares():
        """Under what software to work
        """
        return ["any"]

    def onShow(self):
        """Called when tool pops up
        """
        pass

    def onDestroy(self):
        """Called when tool destroyed
        """
        pass

    def getState(self):
        """Called on tool save and returns a dictionary.

        .. code-block:: python
            :linenos:

            def getState(self):
                state = super().getState()
                # Update state dictionary
                return dict(
                    **state,
                    format=self.format)

        """
        return {"uid": str(self.uid)}

    def restoreState(self, settings):
        """Called when application loaded

        Restore any saved state here.
        Same as **saveState**, settings group already selected, so jsut call **value** method
        to access data

        .. code-block::
            :linenos:

            def restoreState(self, settings):
                super(ScreenshotTool, self).restoreState(settings)
                formatValue = settings.format
                if formatValue is not None:
                    self.format = formatValue
                else:
                    self.format = "PNG"

        :param settings: Settings dictionary
        :type settings: Mapping type
        """
        uidStr = settings.uid
        if uidStr is None:
            self.uid = uuid.uuid4()
        else:
            self.uid = uuid.UUID(uidStr)

    def setAppInstance(self, trailsInstance):
        if self.trailsInstance is None:
            self.trailsInstance = trailsInstance

    @staticmethod
    def toolTip():
        """Tool tip message

        :rtype: str
        """
        return "Default tooltip"

    def uniqueName(self):
        """Tool unique name

        In case if tool is not a singleton like PropertiesTool, we need to
        store separate data for each instance. We use unique identifiers
        (:class:`~uuid.UUID`) postfixes for this.

        The unique name must be a valid identifier, because it is used by the
        configuration system as a key.

        :rtype: str
        """
        return "{0}__{1}".format(self.name().replace(' ', '_'), str(self.uid.hex))

    @staticmethod
    def getName(uniqueName):
        """Name (as returned by `name()`) from `uniqueName`.

        >>> ToolBase.getName(tool.uniqueName()) == tool.name()
        True

        :rtype: str
        """
        name = uniqueName.rsplit('__', maxsplit=1)[0]
        return name.replace('_', ' ')

    @staticmethod
    def name():
        return "ToolBase"


class ShelfTool(ToolBase):
    """Base class for shelf tools
    """
    def __init__(self):
        super(ShelfTool, self).__init__()

    def contextMenuBuilder(self):
        return None

    @staticmethod
    def getIcon():
        return QtGui.QIcon.fromTheme("go-home")

    def do(self):
        print(self.name(), "called!", self.canvas)


class DockTool(QtWidgets.QDockWidget, ToolBase):
    """Base class for dock tools
    """
    def __init__(self):
        ToolBase.__init__(self)
        QtWidgets.QDockWidget.__init__(self)
        self.setToolTip(self.toolTip())
        self.setFeatures(QtWidgets.QDockWidget.AllDockWidgetFeatures)
        self.setAllowedAreas(QtCore.Qt.BottomDockWidgetArea | QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea | QtCore.Qt.TopDockWidgetArea)
        self.setObjectName(self.uniqueName())
        self.setTitleBarWidget(DockTitleBar(self))
        self.setFloating(False)

    @staticmethod
    def defaultDockArea():
        return QtCore.Qt.LeftDockWidgetArea

    @staticmethod
    def isSingleton():
        return False

    def onShow(self):
        super(DockTool, self).onShow()
        self.setWindowTitle(self.name())

    @staticmethod
    def getIcon():
        return None

    def restoreState(self, settings):
        super(DockTool, self).restoreState(settings)
        self.setObjectName(self.uniqueName())

    def closeEvent(self, event):
        self.onDestroy()
        self.parent().unregisterToolInstance(self)
        event.accept()

    def addButton(self, button):
        self.titleBarWidget().addButton(button)


class DockTitleBar(QtWidgets.QWidget):
    def __init__(self, dockWidget, renamable=False):
        super(DockTitleBar, self).__init__(dockWidget)
        self.renamable = renamable
        self.setLayout(QtWidgets.QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 1)
        self.buttonsLay = QtWidgets.QHBoxLayout()
        self.buttonsLay.setSpacing(1)
        self.buttonsLay.setMargin(1)
        self.box = QtWidgets.QGroupBox("")
        self.box.setLayout(self.buttonsLay)
        self.box.setObjectName("Docked")
        self.layout().addWidget(self.box)

        self.box.mouseDoubleClickEvent = self.mouseDoubleClickEvent
        self.box.mousePressEvent = self.mousePressEvent
        self.box.mouseMoveEvent = self.mouseMoveEvent
        self.box.mouseReleaseEvent = self.mouseReleaseEvent

        self.titleLabel = QtWidgets.QLabel(self)
        self.titleLabel.setStyleSheet("background:transparent")
        self.titleEdit = QtWidgets.QLineEdit(self)
        self.titleEdit.hide()
        self.titleEdit.editingFinished.connect(self.finishEdit)

        self.buttonSize = QtCore.QSize(14, 14)

        self.dockButton = QtWidgets.QToolButton(self)
        self.dockButton.setIcon(QtGui.QIcon(':/split_window.png'))
        self.dockButton.setMaximumSize(self.buttonSize)
        self.dockButton.setAutoRaise(True)
        self.dockButton.clicked.connect(self.toggleFloating)

        self.closeButton = QtWidgets.QToolButton(self)
        self.closeButton.setMaximumSize(self.buttonSize)
        self.closeButton.setAutoRaise(True)
        self.closeButton.setIcon(QtGui.QIcon(':/close_window.png'))
        self.closeButton.clicked.connect(self.closeParent)

        self.buttonsLay.addSpacing(2)
        self.buttonsLay.addWidget(self.titleLabel)
        self.buttonsLay.addWidget(self.titleEdit)
        self.buttonsLay.addStretch()
        self.buttonsLay.addSpacing(5)
        self.buttonsLay.addWidget(self.dockButton)
        self.buttonsLay.addWidget(self.closeButton)

        dockWidget.featuresChanged.connect(self.onFeaturesChanged)

        self.onFeaturesChanged(dockWidget.features())
        self.setTitle(dockWidget.windowTitle())
        dockWidget.installEventFilter(self)
        dockWidget.topLevelChanged.connect(self.ChangeFloatingStyle)

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.WindowTitleChange:
            self.setTitle(source.windowTitle())
        return super(DockTitleBar, self).eventFilter(source, event)

    def startEdit(self):
        self.titleLabel.hide()
        self.titleEdit.show()
        self.titleEdit.setFocus()

    def finishEdit(self):
        self.titleEdit.hide()
        self.titleLabel.show()
        self.parent().setWindowTitle(self.titleEdit.text())

    def onFeaturesChanged(self, features):
        if not features & QtWidgets.QDockWidget.DockWidgetVerticalTitleBar:
            self.closeButton.setVisible(
                features & QtWidgets.QDockWidget.DockWidgetClosable)
            self.dockButton.setVisible(
                features & QtWidgets.QDockWidget.DockWidgetFloatable)
        else:
            raise ValueError('vertical title bar not supported')

    def setTitle(self, title):
        self.titleLabel.setText(title)
        self.titleEdit.setText(title)

    def ChangeFloatingStyle(self, state):
        if not state:
            self.box.setStyleSheet(UIThemeManager().stylesheet)
        else:
            self.box.setStyleSheet("""QGroupBox{
                                background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                stop: 0 %s,
                                stop: 0.6 %s,
                                stop: 1.0 %s);}""" % (UIThemeManager().getCssString("ButtonColor"),
                                                      UIThemeManager().getCssString("BgColorBright"),
                                                      UIThemeManager().getCssString("BgColorBright")))

    def update(self, *args, **kwargs):
        self.ChangeFloatingStyle(self.parent().isFloating())
        super(DockTitleBar, self).update(*args, **kwargs)

    def toggleFloating(self):
        self.parent().setFloating(not self.parent().isFloating())

    def closeParent(self):
        self.parent().toggleViewAction().setChecked(False)
        self.parent().close()

    def mouseDoubleClickEvent(self, event):
        if event.pos().x() <= self.titleLabel.width() and self.renamable:
            self.startEdit()
        else:
            # this keeps the normal double-click behaviour
            super(DockTitleBar, self).mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        event.ignore()

    def mousePressEvent(self, event):
        event.ignore()

    def mouseMoveEvent(self, event):
        event.ignore()

    def addButton(self, button):
        button.setAutoRaise(True)
        button.setMaximumSize(self.buttonSize)
        self.buttonsLay.insertWidget(5, button)
