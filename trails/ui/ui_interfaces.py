"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

class IUINode(object):
    """docstring for IUINode."""
    def __init__(self):
        super(IUINode, self).__init__()

    def isReroute(self):
        return False

    def serializationHook(self):
        raise NotImplementedError("serializationHook of IUINode is not implemented")


class IPropertiesViewSupport(object):
    """docstring for IPropertiesViewSupport."""
    def __init__(self):
        super(IPropertiesViewSupport, self).__init__()

    def createPropertiesWidget(self, propertiesWidget):
        pass


class IDataExporter(object):
    """Data exporter/importer

    Editor data can be exported/imported to/from arbitrary formats
    """
    def __init__(self):
        super(IDataExporter, self).__init__()

    @staticmethod
    def creationDateString():
        raise NotImplementedError('creationDateString method of IDataExporter is not implemented')

    @staticmethod
    def version():
        raise NotImplementedError('version method of IDataExporter is not implemented')

    @staticmethod
    def displayName():
        raise NotImplementedError('displayName method of IDataExporter is not implemented')

    @staticmethod
    def toolTip():
        return ''

    @staticmethod
    def createImporterMenu():
        return True

    @staticmethod
    def createExporterMenu():
        return True

    @staticmethod
    def doImport(trailsInstance):
        raise NotImplementedError('doImport method of IDataExporter is not implemented')

    @staticmethod
    def doExport(trailsInstance):
        raise NotImplementedError('doExport method of IDataExporter is not implemented')


class IPackage(object):
    """Class that describes a set of modules that can be plugged into the editor.

    Will be instantiated and used to create registered entities.
    """
    def __init__(self):
        super(IPackage, self).__init__()

    @staticmethod
    def GetExporters():
        """Registered editor data exporters

        :rtype: dict(str, class)
        """
        raise NotImplementedError('GetExporters method of IPackage is not implemented')

    @staticmethod
    def GetFunctionLibraries():
        """Registered function library instances

        :rtype: dict(str, object)
        """
        raise NotImplementedError('GetFunctionLibraries method of IPackage is not implemented')

    @staticmethod
    def GetNodeClasses():
        """Registered node classes

        :rtype: dict(str, class)
        """
        raise NotImplementedError('GetNodeClasses method of IPackage is not implemented')

    @staticmethod
    def GetPinClasses():
        """Registered pin classes

        :rtype: dict(str, class)
        """
        raise NotImplementedError('GetPinClasses method of IPackage is not implemented')

    @staticmethod
    def GetToolClasses():
        """Registered tool classes

        :rtype: dict(str, class)
        """
        raise NotImplementedError('GetToolClasses method of IPackage is not implemented')

    @staticmethod
    def UIPinsFactory():
        """Registered ui pin wrappers

        :rtype: function
        """
        return None

    @staticmethod
    def UINodesFactory():
        """Registered ui nodes

        :rtype: function
        """
        return None

    @staticmethod
    def PinsInputWidgetFactory():
        """Registered pin input widgets

        :rtype: function
        """
        return None

    @staticmethod
    def PrefsWidgets():
        """Registered preferences widgets

        :rtype: dict(str, class)
        """
        return None
