"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

import os
import pathlib
import platform

import platformdirs
import upsilonconf
import step

from Qt.QtGui import QColor
from Qt.QtWidgets import QApplication

from trails.core.version import Version
from trails.core.common import SingletonDecorator
from trails.input import InputAction


DEFAULT_EDITOR = (
    os.environ.get('VISUAL')
    or os.environ.get('EDITOR')
    or {"Linux": "gedit",
        "Darwin": "TextEdit",
        "Java": None,
        "Windows": "notepad.exe"
        }.get(platform.system(), None)
    )

APPDIR = pathlib.Path(__file__).parent

class PlatformPaths(platformdirs.PlatformDirs):
    """Subclass `PlatformDirs` to get all dirs/paths and not just first.

    This adds four new properties:
        - `site_data_dirs`:
            Returns all site data dirs, independent of the value of the
            `multipath` setting.
        - `site_config_dirs`:
            Returns all site config dirs, independent of the value of the
            `multipath` setting.
        - `site_data_paths`:
            Returns a list with all site data paths, independent of the value
            of the `multipath` setting.
        - `site_config_paths`:
            Returns a list with all site config paths, independent of the
            value of the `multipath` setting.

    """

    @property
    def site_data_dirs(self):
        """Returns all site data dirs, independent of the `multipath` value."""
        multipath = self.multipath
        self.multipath = True

        dirs = self.site_data_dir

        self.multipath = multipath
        return dirs

    @property
    def site_config_dirs(self):
        """Returns all site config dirs, independent of the `multipath` value."""
        multipath = self.multipath
        self.multipath = True

        paths = self.site_config_dir

        self.multipath = multipath
        return paths

    @property
    def site_data_paths(self):
        """Returns a list with all site data dirs, independent of the `multipath` value."""
        return [platformdirs.Path(p)
                for p in self.site_data_dirs.split(platformdirs.os.pathsep)]

    @property
    def site_config_paths(self):
        """Returns a list with all site config dirs, independent of the `multipath` value."""
        return [platformdirs.Path(p)
                for p in self.site_config_dir.split(platformdirs.os.pathsep)]


PLATFORMPATHS = PlatformPaths(appname="trails")


class _ConfigManager(upsilonconf.Configuration):

    fname = None
    DEFAULTS = {}

    @property
    def version(self):
        return self._version

    def __init__(self):
        super().__init__()
        self._version = None
        self.load()

    def _reset(self, data):
        self.clear()
        self.overwrite_all(**data)

    def reset(self):
        self._reset(self.DEFAULTS)

    def _load(self, fname):
        data = upsilonconf.Configuration()
        if fname is None:
            paths = [
                p.joinpath(self.fname)
                for p in (
                    *PLATFORMPATHS.site_config_paths,   # Site configuration
                    PLATFORMPATHS.user_config_path,     # User configuration
                    pathlib.Path("."))]                 # Current directory
        else:
            paths = [pathlib.Path(fname)]
        for path in paths:
            try:
                data.overwrite_all(upsilonconf.load(path))
            except FileNotFoundError:
                pass
            except:
                # Recover, if the file is empty
                if path.stat().st_size == 0:
                    pass
                else:
                    raise
        if "Version" in data:
            # Convert version string to Version object
            self._version = Version(data.pop("Version"))
        return data

    def load(self, fname=None):
        self.reset()
        self.overwrite_all(self._load(fname=fname))

    def _save(self, data):
        # Inject version string
        # `data` can be a dictionary or a Configuration object!
        if isinstance(data, dict):
            data["Version"] = str(Version())
        else:
            data.overwrite("Version", str(Version()))

        # Save
        localfile = pathlib.Path(".", self.fname)
        if localfile.exists():
            upsilonconf.save(data, localfile)
        else:
            upsilonconf.save(
                data,
                PLATFORMPATHS.user_config_path.joinpath(self.fname))

    def save(self):
        self._save(self)

    def inject(self, group, **kwargs):
        try:
            conf = self[group]
        except KeyError:
            if group is None:
                conf = self
            else:
                self[group] = {}
                conf = self[group]

        for key, value in kwargs.items():
            if key not in conf:
                conf[key] = value


class _ThemeManager(_ConfigManager):

    subdir = ()

    def __getitem__(self, key):
        # Return `None`, if key does not exist
        try:
            return super().__getitem__(key)
        except KeyError:
            if super().__getattr__("name") is None:
                return None
            else:
                return super().__getattr__("theme").get(key)

    def __getattr__(self, name):
        # Return `None`, if name does not exist
        try:
            return super().__getattr__(name)
        except AttributeError:
            if super().__getattr__("name") is None:
                return None
            else:
                return getattr(super().__getattr__("theme"), name)

    def __init__(self):
        super().__init__()
        self._appInstance = None

    def setAppInstance(self, appInstance):
        self._appInstance = appInstance

    def clear(self):
        super().clear()
        # This trick is needed to set 'themes' to a dictionary and not an
        # upsilonconf.Configuration object!
        self.themes = {None: None}
        self.themes.pop(None)
        self.name = None
        self.theme = None

    def load(self):
        self.clear()
        # Find all themes
        for path in (
                APPDIR.joinpath("ui", *self.subdir),
                *[p.joinpath(*self.subdir) for p in PLATFORMPATHS.site_config_paths],
                PLATFORMPATHS.user_config_path.joinpath(*self.subdir)):
            if path.exists():
                for p in path.iterdir():
                    if p.is_file() and p.suffix == ".yaml":
                        data = upsilonconf.load(p)
                        # Convert colors to QColor() ...
                        colors = {}
                        for key, value in data.Theme.items():
                            if isinstance(value, str):
                                colors[key] = QColor(value)
                            elif isinstance(value, (list, tuple)):
                                colors[key] = QColor(*value)
                        # ... and update style sheet
                        data.Theme.overwrite_all(**colors)
                        self.themes[data.Name] = data

    def save(self):
        raise NotImplementedError(
            f"'{self.__class__.__name__}.save()' is not implemented yet. "
            f"Theme is not saved.")

    def select(self, themename):
        if themename is None:
            self.overwrite('name', None)
            self.overwrite('theme', None)
        else:
            self.overwrite('name', themename)
            self.overwrite('theme', self.themes[themename].Theme)

    def getCssString(self, name):
        color = self[name]
        if color is None:
            return "None"
        else:
            return "rgba%s" % str(color.getRgb())

    def updateApp(self):

        if self._appInstance is None:
            return

        if self._appInstance.currentSoftware == "standalone":
            topWindow = QApplication.instance()
        else:
            topWindow = self._appInstance

        self.updateTopWindow(topWindow)

    def updateTopWindow(self, topWindow):
        raise NotImplementedError(
            f"'{self.__class__.__name__}.updateTopWindow() is not implemented.")


@SingletonDecorator
class PreferencesManager(_ConfigManager):

    fname = "trails.yaml"
    DEFAULTS = {
        "General": {
            "ExtraPackageDirs": [os.path.join(PLATFORMPATHS.user_data_dir, "packages")],
            "EditorCmd": f"{DEFAULT_EDITOR} @FILE",
            "HistoryDepth": 50,
            "AutoZoom": True,
            "AdjAutoZoom": 1,
            "InitialZoom": 1,
            "RedirectOutput": False,
            },
        "UI": {
            "Theme": None
            },
        "Graph": {
            "Theme": "Default",
            # TODO: Add these (and all non-color) configurations to the
            #   preferences dialog?
            # GridDraw
            # GridDrawNumbers
            # ConnectionMode
            # ConnectionRoundness
            }
        }


@SingletonDecorator
class AppStateManager(_ConfigManager):

    fname = "state.yaml"
    DEFAULTS = {
        "Editor": {
            "geometry": None,
            "state": None
            },
        "Tools": {
            "ShelfTools": {},
            "DockTools": {}
            }
        }


@SingletonDecorator
class GraphThemeManager(_ThemeManager):

    subdir = ("themes", "graph")

    def updateTopWindow(self, topWindow):
        for widget in topWindow.allWidgets():
            widget.update()


@SingletonDecorator
class UIThemeManager(_ThemeManager):
    # As `GraphThemeManager`, but with stylesheet and `None` theme (the
    # system's default display)

    subdir = ("themes", "ui")

    def clear(self):
        super().clear()
        self.themes[None] = None
        self.stylesheet = ""

    def select(self, themename):
        if themename == self.name:
            # No change, avoid reading the style sheet file
            return

        if themename is None:
            self.overwrite('stylesheet', "")
        else:
            # Find stylesheet template
            sheet = pathlib.Path(self.themes[themename].StyleSheet)
            if sheet.is_file() and sheet.parent != pathlib.Path('.'):
                # File exists, is not a directory and is specified with a path
                pass
            else:
                # Find style sheet in directories
                for path in (
                        APPDIR.joinpath("ui", *self.subdir),
                        *[p.joinpath(*self.subdir) for p in PLATFORMPATHS.site_config_paths],
                        PLATFORMPATHS.user_config_path.joinpath(*self.subdir)):
                    if path.joinpath(sheet).is_file():
                        sheet = path.joinpath(sheet)
                        break
                else:
                    raise FileNotFoundError(
                        f"Style sheet '{sheet}' cannot be found.")
            # Load style sheet
            with open(sheet) as f:
                style = f.read()
            # Set values
            self.overwrite('stylesheet', step.Template(style).expand(
                **{name: f'rgba{color.getRgb()}'
                    for name, color in self.themes[themename].Theme.items()}))
        super().select(themename)

    def updateTopWindow(self, topWindow):
        topWindow.setStyleSheet(self.stylesheet)
        for widget in topWindow.allWidgets():
            widget.update()


@SingletonDecorator
class ShortcutsManager(_ConfigManager):

    fname = "shortcuts.yaml"
    DEFAULTS = {
        "Canvas": {
            "Navigation": {
                "Pan": [
                    {"name": "Canvas.Pan",
                     "mouse": "MiddleButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": []},
                    {"name": "Canvas.Pan",
                     "mouse": "LeftButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": [
                        "Alt+"]}],
                "Zoom": [
                    {"name": "Canvas.Zoom",
                     "mouse": "RightButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": []}],
                "FrameSelected": [
                    {"name": "Canvas.FrameSelected",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "F",
                     "modifiers": []}],
                "FrameAll": [
                    {"name": "Canvas.FrameAll",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "H",
                     "modifiers": []}],
                "ZoomIn": [
                    {"name": "Canvas.ZoomIn",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "=",
                     "modifiers": [
                        "Ctrl+"]}],
                "ZoomOut": [
                    {"name": "Canvas.ZoomOut",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "-",
                     "modifiers": [
                        "Ctrl+"]}],
                "ResetScale": [
                    {"name": "Canvas.ResetScale",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "R",
                     "modifiers": [
                        "Ctrl+"]}]
                },
            "Refactoring": {
                "AlignLeft": [
                    {"name": "Canvas.AlignLeft",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Left",
                     "modifiers": [
                        "Shift+",
                        "Ctrl+"]}],
                "AlignTop": [
                    {"name": "Canvas.AlignTop",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Up",
                     "modifiers": [
                        "Shift+",
                        "Ctrl+"]}],
                "AlignRight": [
                    {"name": "Canvas.AlignRight",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Right",
                     "modifiers": [
                        "Shift+",
                        "Ctrl+"]}],
                "AlignBottom": [
                    {"name": "Canvas.AlignBottom",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Down",
                     "modifiers": [
                        "Shift+",
                        "Ctrl+"]}]
                },
            "Editing": {
                "Undo": [
                    {"name": "Canvas.Undo",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Z",
                     "modifiers": [
                        "Ctrl+"]}],
                "Redo": [
                    {"name": "Canvas.Redo",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Y",
                     "modifiers": [
                        "Ctrl+"]}],
                "KillSelected": [
                    {"name": "Canvas.KillSelected",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "Del",
                     "modifiers": []}],
                "CopyNodes": [
                    {"name": "Canvas.CopyNodes",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "C",
                     "modifiers": [
                        "Ctrl+"]}],
                "CutNodes": [
                    {"name": "Canvas.CutNodes",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "X",
                     "modifiers": [
                        "Ctrl+"]}],
                "DragCopyNodes": [
                    {"name": "Canvas.DragCopyNodes",
                     "mouse": "LeftButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": [
                        "Alt+"]},
                    {"name": "Canvas.DragCopyNodes",
                     "mouse": "MiddleButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": [
                        "Alt+"]}],
                "DragNodes": [
                    {"name": "Canvas.DragNodes",
                     "mouse": "MiddleButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": []},
                    {"name": "Canvas.DragNodes",
                     "mouse": "LeftButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": []}],
                "DragChainedNodes": [
                    {"name": "Canvas.DragChainedNodes",
                     "mouse": "MiddleButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": []}],
                "PasteNodes": [
                    {"name": "Canvas.PasteNodes",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "D",
                     "modifiers": [
                        "Ctrl+"]}],
                "DuplicateNodes": [
                    {"name": "Canvas.DuplicateNodes",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "D",
                     "modifiers": [
                        "Ctrl+"]}],
                "DisconnectPin": [
                    {"name": "Canvas.DisconnectPin",
                     "mouse": "LeftButton",
                     "actionType": "Mouse",
                     "key": None,
                     "modifiers": [
                        "Alt+"]}]
                },
            },
        "App": {
            "IO": {
                "NewFile": [
                    {"name": "App.NewFile",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "N",
                     "modifiers": [
                        "Ctrl+"]}],
                "Save": [
                    {"name": "App.Save",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "S",
                     "modifiers": [
                        "Ctrl+"]}],
                "SaveAs": [
                    {"name": "App.SaveAs",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "S",
                     "modifiers": [
                        "Shift+",
                        "Ctrl+"]}],
                "Open": [
                    {"name": "App.Open",
                     "mouse": "NoButton",
                     "actionType": "Keyboard",
                     "key": "O",
                     "modifiers": [
                        "Ctrl+"]}]
                }
            }
        }

    def toYAML(self):
        # Convert `InputAction`s to YAML data
        data = {}
        for actionArea, actionGroups in self.items():
            data[actionArea] = {}
            for actionGroup, actions in actionGroups.items():
                data[actionArea][actionGroup] = {}
                for actionName, actionVariants in actions.items():
                    data[actionArea][actionGroup][actionName] = [
                        actionVariant.toYAML()
                        for actionVariant in actionVariants]
        return data

    def fromYAML(self, yamldata):
        # Convert YAML data to `InputAction`s
        data = upsilonconf.Configuration()
        for actionArea, actionGroups in yamldata.items():
            data.overwrite(actionArea, {})
            for actionGroup, actions in actionGroups.items():
                data[actionArea].overwrite(actionGroup, {})
                for actionName, actionVariants in actions.items():
                    data[actionArea][actionGroup].overwrite(
                        actionName,
                        [InputAction.fromYAML(variant)
                         for variant in actionVariants])
        return data

    def reset(self):
        # TODO: fromYAML()!!
        self._reset(self.fromYAML(self.DEFAULTS))

    def load(self):
        self.reset()

        data = self._load(fname=None)
        # Convert YAML data to `InputAction`'s
        if data:
            self.overwrite_all(self.fromYAML(data))

    def save(self):
        # Save the data in YAML format
        self._save(self.toYAML())
