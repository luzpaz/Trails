"""
Copyright (C) 2015-2019  Ilgar Lunin, Pedro Cabrera
Copyright (C) 2019  Thomas Gundermann
Copyright (C) 2022  Stephan Helma

This file is part of Trails.

Trails is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

Trails is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Trails. If not, see <https://www.gnu.org/licenses/>.

"""

from collections import Counter
from collections import defaultdict

from Qt import QtCore, QtGui

import enum


class InputActionType(enum.Enum):
    Mouse = 1
    Keyboard = 2


class InputAction(object):
    def __init__(self, name, actionType, mouse=QtCore.Qt.NoButton, key=None, modifiers=QtCore.Qt.NoModifier):
        self.__actionType = actionType
        self._name = name
        self.__data = {"mouse": mouse, "key": key, "modifiers": modifiers}

    def __str__(self):
        return "{0} {1} {2}".format(QtGui.QKeySequence(self.getModifiers()).toString(),
                                    self.getMouseButton().name.decode('utf-8'),
                                    QtGui.QKeySequence(self.getKey()).toString())

    @property
    def actionType(self):
        return self.__actionType

    def __eq__(self, other):
        sm = self.__data["mouse"]
        sk = self.__data["key"]
        smod = self.__data["modifiers"]
        om = other.getData()["mouse"]
        ok = other.getData()["key"]
        omod = other.getData()["modifiers"]
        smod == omod
        return all([sm == om,
                    sk == ok,
                    smod == omod])

    def __ne__(self, other):
        sm = self.__data["mouse"]
        sk = self.__data["key"]
        smod = self.__data["modifiers"]
        om = other.getData()["mouse"]
        ok = other.getData()["key"]
        omod = other.getData()["modifiers"]
        return not all([sm == om,
                        sk == ok,
                        smod == omod])

    def getName(self):
        return self._name

    def getData(self):
        return self.__data

    def setMouseButton(self, btn):
        assert(isinstance(btn, QtCore.Qt.MouseButton))
        self.__data["mouse"] = btn

    def getMouseButton(self):
        return self.__data["mouse"]

    def setKey(self, key=None):
        if isinstance(key, QtCore.Qt.Key):
            self.__data["key"] = int(key)

    def getKey(self):
        return self.__data["key"]

    def setModifiers(self, modifiers=QtCore.Qt.NoModifier):
        self.__data["modifiers"] = modifiers

    def getModifiers(self):
        return self.__data["modifiers"]

    def toYAML(self):
        yamlData = {}
        yamlData["name"] = self._name

        qMouse = self.__data["mouse"]
        mouse = qMouse.name
        if mouse is None:
            mouse = int(qMouse)
        else:
            mouse = mouse.decode("utf-8")
        yamlData["mouse"] = mouse

        yamlData["actionType"] = self.actionType.name

        qKey = self.__data["key"]
        if qKey is None:
            key = None
        else:
            key = QtGui.QKeySequence(qKey).toString()
        yamlData["key"] = key

        result = []
        mods = self.__data["modifiers"]
        if mods & QtCore.Qt.ShiftModifier:
            result.append(QtGui.QKeySequence(QtCore.Qt.ShiftModifier).toString())
        if mods & QtCore.Qt.ControlModifier:
            result.append(QtGui.QKeySequence(QtCore.Qt.ControlModifier).toString())
        if mods & QtCore.Qt.AltModifier:
            result.append(QtGui.QKeySequence(QtCore.Qt.AltModifier).toString())
        if mods & QtCore.Qt.MetaModifier:
            result.append(QtGui.QKeySequence(QtCore.Qt.MetaModifier).toString())
        if mods & QtCore.Qt.KeypadModifier:
            result.append(QtGui.QKeySequence(QtCore.Qt.KeypadModifier).toString())
        if mods & QtCore.Qt.GroupSwitchModifier:
            result.append(QtGui.QKeySequence(QtCore.Qt.GroupSwitchModifier).toString())
        yamlData["modifiers"] = result

        return yamlData

    @classmethod
    def fromYAML(cls, yamlData):
        try:
            name = yamlData["name"]

            mouse = yamlData["mouse"]
            if isinstance(mouse, str):
                qMouse = getattr(QtCore.Qt.MouseButton, mouse)
            else:
                qMouse = QtCore.Qt.MouseButton(mouse)

            key = yamlData["key"]
            if isinstance(key, int):
                qKey = key
            elif isinstance(key, str):
                qKey = QtGui.QKeySequence.fromString(key)[0]
            else:
                qKey = None

            _modifiers = {
                QtGui.QKeySequence(getattr(QtCore.Qt, modifier)).toString():
                    getattr(QtCore.Qt, modifier)
                for modifier in dir(QtCore.Qt.KeyboardModifier)
                if modifier.endswith("Modifier")}
            mods = QtCore.Qt.NoModifier
            for mod in yamlData["modifiers"]:
                if isinstance(mod, int):
                    mods |= mod
                else:
                    mods |= _modifiers[mod]

            action = yamlData["actionType"]
            if isinstance(action, int):
                actionType = InputActionType(action)
            else:
                actionType = InputActionType[action]
            return cls(name, actionType, mouse=qMouse, key=qKey, modifiers=mods)

        except:
            return None
