# Dev-branch

## Release highlights
## Added
## Changed
## Deprecated
## Removed
## Fixed
## Security
## Known bugs
## How to update


# v3.0.0a1 (2022-08-05)

## Release highlights

- Compatibility with Python 2 has been dropped!
  (Sorry, but Python 2 has reached its
  [end of life with 2020](https://www.python.org/doc/sunset-python-2/)), more
  than 2½ years ago). The minimum Python version is now 3.6.
- The standalone window is now displayed correctly on High DPI screens.
- The program `trails.py` can be called with command line parameters (start it
  with `-h` or `--help` to see all available parameters).
- The configuration system was reworked:
    - The configuration files are now in the human-readable YAML format.
    - The configurations files are:
        - `trails.yaml`: Main configuration file
        - `shortcuts.yaml`: Keyboard/mouse shortcuts
        - `state.yaml`: The window state, when the program was closed
    - All three configuration files are read from the following locations (in
      that order):
        1. System configuration file, e.g. `/etc`.
        2. User configuration file, e.g. `~/.config/trails/`.
        3. File in directory, where PyFlow/Trails is started from, `./`.
      Settings from later files take precedence over settings read in earlier.
      Command line parameters overrule all settings read from files. If a
      configuration file is specified at the command line with the `-c` or
      `--config` option, all other configuration files are ignored.
    - The configuration file `shortcuts.yaml` now accepts also characters and
      strings as shortcut specifiers for "key", "mouse", "modifiers" and
      "actionType". (Previously the specifier had to be its integer
      representation.)
    - The configuration setting `General.ExtraPackageDirs` is changed from a
      path string (`"PATH1:PATH2"`) to a proper list (`["PATH1", "PATH2"]'). It
      can now be modified graphically in the preferences dialog. The user's
      home directory is now replaced with a `~` (tilde) to make the setting
      independent of the operating system (thus the configuration file can be
      easily moved between installations).
- The theme system was reworked:
    - There are now separate themes for the GUI and the graph area.
    - A plain theme for the GUI was added.
    - A simple ('plain') theme was added for the graph area.
    - The theme system now uses "[step-template](https://github.com/dotpy/step/)"
      to generate the theme stylesheets. Thus it should be easier to make new
      themes.
    - Unfortunately these changes removes the ability to edit themes in the
      preferences editor.
- The preferences dialog was overhauled.
- The "Create Package..." wizard was stream lined.
- The pin's watch text (right click on a pin and select "Watch") is actually
  displayed.
- Tooltips were added to items in the nodes tool box.
- The license was changed from Apache 2 to LGPL 3 or later.

## Added

- CLI:
    - Adds command line parameters to `pyflow.py`/`trails.py`. (Stephan Helma)
- GUI:
    - Adds a plain theme. (Stephan Helma)
    - Adds drop on wire functionality (R. Scharf-Wildenhain,
      https://github.com/wonderworks-software/PyFlow/pull/110/commits/33564ae08508a7f393d288e37f7c74ba8eb4bd45)
    - Adds `dict` type to UI. (Cemsina Güzel,
      https://github.com/wonderworks-software/PyFlow/pull/97)
    - Adds tooltips to items in `NodeBox`. (Stephan Helma)
    - Adds UI improvements regarding buttons etc. (Stephan Helma)
    - Adds clear button to search boxes. (Stephan Helma)
- API:
    - When importing Trails as a widget, it can be configured with the help of
      the `PreferencesManager` (that is how the command line parameters work).
      (Stephan Helma)
- Source:
    - Adds copyright line to files for each contributor. (Stephan Helma)
    - Adds left out copyright line for authors contributing code through
      https://github.com/wonderworks-software/PyFlow/pull/. (Stephan Helma)

## Changed

- Python:
    - Removes support for Python 2. (Stephan Helma)
    - Increases minimum Python version to 3.6, because f-strings are used.
      (Stephan Helma)
- Configuration system: INCOMPATIBLE
    - Uses YAML format for configuration files. (Stephan Helma)
    - Load configuration files from these locations (Stephan Helma):
        1. System configuration file.
        2. User configuration file.
        3. File in directory, where PyFlow/Trails is started from.
    - Configurations files are now named (Stephan Helma):
        - `pyflow.yaml`/`trails.yaml`: Main configuration file
        - `shortcuts.yaml`: Keyboard/mouse shortcuts
        - `state.yaml`: The window state, when the program was closed
    - `shortcuts.yaml` now accepts also characters and strings for "key",
      "mouse", "modifiers" and "actionType" instead of only their integer
      representations. (Stephan Helma)
    - Defaults are saved in the preferences' class, which are derived from
      `CategoryWidgetBase` (Stephan Helma).
    - Changes the configuration `General.ExtraPackageDirs` from a path string
      to a proper list which can be modified graphically in the preference.
      dialog. It also replaces the user's home directory with a `~` (tilde) to
      make them independent of the operating system (so that the configuration
      file can be moved between installations). (Stephan Helma)
    - The PyFlow/Trails version a configuration file was saved under, is
      available as a read-only property of the corresponding manager as
      `...Manager().version`: (Stephan Helma).
- Themes:
    - Separates themes for the GUI and the graph area. (Stephan Helma)
    - Uses "step-template" (https://github.com/dotpy/step/) to generate the
      theme stylesheets. (Stephan Helma)
    - Unfortunately removes possiblity to edit themes in preferences dialog.
      (Stephan Helma)
- CLI:
    - Adds she-bangs and makes `trails.py` and `trails_run.py` executable.
- GUI:
    - Decreases size of pin's watch text and actually displays it. (Stephan
      Helma)
    - Reworks the "Create Package..." Wizard:
        - It is now possible to select the package path from the directories
          specified in the `General.ExtraPackageDirs` settings. (Stephan Helma)
        - Optionally adds the package path to the `General.ExtraPackageDirs`,
          if it is missing. (Stephan Helma)
        - Optionally opens the file manager on finish. (Stephan Helma)
        - Enforces CamelCase package name and converts them to snake_case
          subdirectory names. Package names can contain numbers. (Stephan
          Helma)
        - Cosmetic changes and improvements. (Stephan Helma)
        - More flexible API. (Stephan Helma)
    - Adds new application icon. (Stephan Helma)
- API:
    - Uses `tempfile.mkdtemp()` to generate temporary directory (instead of the
      old custom function). Also removes the `General.TempFilesDir`
      configuration option. (Stephan Helma)
    - Changes directory names from 'CamelCase' to 'snake_case' according to
      Python conventions. (Stephan Helma)
    - Renames `InputManager` to `ShortcutsManager`, `InputPreferences` to
      `ShortcutsPreferences` and `input.yaml` to `shortcuts.yaml`. (Stephan
      Helma)
- Source:
    - Changes 'CHANGELOG.rst' to 'CHANGELOG.md'. (Stephan Helma)
    - Changes from Apache 2 to LGPL 3 or later. (Stephan Helma)

## Removed

- GUI:
    - Removes ability to edit themes in the preferences dialog. (Stephan Helma)
- Source:
    - Removes dependency on `docutils`:
        - Removes support for ReStructuredText in nodes' `description()`.
          (Stephan Helma)
        - Removes support for ReStructuredText in nodes' doc-strings. (Stephan
          Helma)
    - Removes `# -*- coding: utf-8 -*-`, because in Python3 all files are
      utf-8. (Stephan Helma)

## Fixed

- CLI:
    - PyFlow/Trails can be killed with `SIGINT`, e.g. `Ctrl-C` from the command
      line. (Stephan Helma).
- GUI:
    - Corrects the display on High DPI screens. (Stephan Helma)
    - Displays an error message, if the configured editor command cannot be
      found. (Stephan Helma)
    - Fixes some UI glitches. (Cristina Gabriela Gavrila,
      https://github.com/wonderworks-software/PyFlow/commit/4c8bc6ca999d2b855bbfd5e3b8e4aef99125647b)
    - Fixes more UI glitches. (Abdelaziz Sharaf,
      https://github.com/wonderworks-software/PyFlow/pull/107)
    - Enforces new package name in wizard to be in CamelCase with numbers.
      (Stephan Helma)
- API:
    - Fixes splitting of the paths in the `TRAILS_PACKAGES_PATHS` environmental
      variable. (Stephan Helma)
- Source:
    - Corrects typo `PinSpecifires` to `PinSpecifiers`. (Stephan Helma)
    - Corrects typo in comment. (David Vlaminck,
      https://github.com/wonderworks-software/PyFlow/pull/105)
    - Fixes bug of graph base for more parameters (biek,
      https://github.com/wonderworks-software/PyFlow/commit/9d4a331e1454c6d7d2b9d03257b28e4acf8cb95b)
    - Uses immutable objects directly instead of trying to copy them. (Cristina
      Gabriela Gavrila,
      https://github.com/wonderworks-software/PyFlow/commit/ff22da310507fa9a4244e0acce7c59231deead19)

## Known bugs

- GUI:
    - The context menu pops up, if the right mouse button is captured on a
      `MouseButtonCaptureWidget`. (PyFlow/UI/Widgets/MouseButtonCapture.py L69)
- API:
    - The version string is not saved in the themes files!
    - The version stored in configuration files is not used yet.
- Packaging:
    - The CLI interface is not registered in `[project.gui-scripts]`.

## How to update

- Replace ReStructuredText in nodes' descriptions with HTML text.
- Replace ReStructuredText in nodes' doc-strings with HTML text.
- Replace `PyFlow` imports with `trails` imports.
- Replace 'CamelCase' imports with 'snake_case' imports.
- Replace 'CamelCase' resources with 'snake_case' names.
- Remove import of the `nine` module.

In this release some incompatible changes were introduced, such as:
- configuration file format
- themes

but look also out for
- `Tools.saveState`, `Tools.restoreState`, `Tools.PREFERENCES_DEFAULTS`
- Version objects: `fromString()` and `currentVersion()` are removed, because
  `Version()` can be called with a string (equivalent to `fromString()`) and
  without any argument (equivalent to `currentVersion()`).


# v2.0.1 (2019-09-21)

- LOD settings added to preferences
- Themes improvements
- New wire styles
- Api refactoring
- App can run without packages
- Sliders improvements and refactoring
- Smart create input widgets for int and floats in function library, if range
  is specified

Bug fixes


# v2.0.0 (2019-06-11)

- Subgraphs added
- Node anatomy changed
- Added data exporters/importers hooks
- Everything is modular
- Editor tool class added
- Customizable themes
- Enhanced wire connection
- Wires are selectable
- All nodes can be collapsed
- Node's menu actions can be showed to the right of node name as buttons
